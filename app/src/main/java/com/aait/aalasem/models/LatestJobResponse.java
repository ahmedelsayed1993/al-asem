package com.aait.aalasem.models;

public class LatestJobResponse extends BaseResponse {
    private GetLatestJobModel data;

    public GetLatestJobModel getData() {
        return data;
    }

    public void setData(GetLatestJobModel data) {
        this.data = data;
    }
}
