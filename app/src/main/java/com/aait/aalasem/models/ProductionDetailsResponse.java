package com.aait.aalasem.models;

public class ProductionDetailsResponse extends BaseResponse {
    private ProductionModel data;

    public ProductionModel getData() {
        return data;
    }

    public void setData(ProductionModel data) {
        this.data = data;
    }
}
