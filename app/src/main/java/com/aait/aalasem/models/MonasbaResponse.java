package com.aait.aalasem.models;

public class MonasbaResponse extends BaseResponse {
    private MonasbaModel data;

    public MonasbaModel getData() {
        return data;
    }

    public void setData(MonasbaModel data) {
        this.data = data;
    }
}
