package com.aait.aalasem.models;

public class NewDetailsResponse extends BaseResponse {
    private NewsModel data;

    public NewsModel getData() {
        return data;
    }

    public void setData(NewsModel data) {
        this.data = data;
    }
}
