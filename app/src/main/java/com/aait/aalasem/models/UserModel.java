package com.aait.aalasem.models;

import java.io.Serializable;

public class UserModel implements Serializable {
    private UserDataModel user;
    private CodeModel code;

    public UserDataModel getUser() {
        return user;
    }

    public void setUser(UserDataModel user) {
        this.user = user;
    }

    public CodeModel getCode() {
        return code;
    }

    public void setCode(CodeModel code) {
        this.code = code;
    }
}
