package com.aait.aalasem.models;

public class PatientDatailsResponse extends BaseResponse {
    private PacientModel data;

    public PacientModel getData() {
        return data;
    }

    public void setData(PacientModel data) {
        this.data = data;
    }
}
