package com.aait.aalasem.models;

public class MonasbatResponse extends BaseResponse {
    private GetMonasbaModel data;

    public GetMonasbaModel getData() {
        return data;
    }

    public void setData(GetMonasbaModel data) {
        this.data = data;
    }
}
