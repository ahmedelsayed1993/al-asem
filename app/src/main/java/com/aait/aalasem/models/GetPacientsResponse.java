package com.aait.aalasem.models;

public class GetPacientsResponse extends BaseResponse {
    private GetPacientsModel data;

    public GetPacientsModel getData() {
        return data;
    }

    public void setData(GetPacientsModel data) {
        this.data = data;
    }
}
