package com.aait.aalasem.models;

import java.io.Serializable;

public class CheckCodeModel implements Serializable {
    private int userId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
