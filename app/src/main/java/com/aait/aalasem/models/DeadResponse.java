package com.aait.aalasem.models;

import java.util.ArrayList;

public class DeadResponse extends BaseResponse {
    private DeadsModel data;

    public DeadsModel getData() {
        return data;
    }

    public void setData(DeadsModel data) {
        this.data = data;
    }
}
