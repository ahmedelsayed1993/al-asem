package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class NewModel implements Serializable {
    private String word;
    private ArrayList<NewsModel> news;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public ArrayList<NewsModel> getNews() {
        return news;
    }

    public void setNews(ArrayList<NewsModel> news) {
        this.news = news;
    }
}
