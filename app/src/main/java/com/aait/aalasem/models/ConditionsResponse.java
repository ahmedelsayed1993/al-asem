package com.aait.aalasem.models;

public class ConditionsResponse extends BaseResponse {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
