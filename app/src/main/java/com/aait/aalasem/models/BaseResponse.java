package com.aait.aalasem.models;

import java.io.Serializable;

public class BaseResponse implements Serializable {
    private String status;
    private int code;
    private String msg;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
    public boolean issucessfull(){
        if (status.equals("1")){
            return true;
        }else {
            return false;
        }
    }
}
