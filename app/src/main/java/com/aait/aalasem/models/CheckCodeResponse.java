package com.aait.aalasem.models;

public class CheckCodeResponse extends BaseResponse {
    private CheckCodeModel data;

    public CheckCodeModel getData() {
        return data;
    }

    public void setData(CheckCodeModel data) {
        this.data = data;
    }
}
