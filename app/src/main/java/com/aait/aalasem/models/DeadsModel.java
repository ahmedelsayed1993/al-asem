package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class DeadsModel implements Serializable {
    private ArrayList<DeadModel> deads;

    public ArrayList<DeadModel> getDeads() {
        return deads;
    }

    public void setDeads(ArrayList<DeadModel> deads) {
        this.deads = deads;
    }
}
