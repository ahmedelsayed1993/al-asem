package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class DictionaryResponse extends BaseResponse{
    private ArrayList<UserDataModel> data;

    public ArrayList<UserDataModel> getData() {
        return data;
    }

    public void setData(ArrayList<UserDataModel> data) {
        this.data = data;
    }
}
