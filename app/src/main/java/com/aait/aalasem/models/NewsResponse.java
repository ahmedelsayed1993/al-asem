package com.aait.aalasem.models;

public class NewsResponse extends BaseResponse {
    private NewModel data;

    public NewModel getData() {
        return data;
    }

    public void setData(NewModel data) {
        this.data = data;
    }
}
