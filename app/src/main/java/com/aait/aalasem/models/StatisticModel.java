package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class StatisticModel implements Serializable {
    private String count_people_in_tribe;
    private String count_men;
    private String count_women;
    private String married_men;
    private String married_women;
    private String graduate_men;
    private String graduate_women;
    private String single_men;
    private String single_women;
    private ArrayList<StatisticsModel> statistics;

    public String getCount_people_in_tribe() {
        return count_people_in_tribe;
    }

    public void setCount_people_in_tribe(String count_people_in_tribe) {
        this.count_people_in_tribe = count_people_in_tribe;
    }

    public String getCount_men() {
        return count_men;
    }

    public void setCount_men(String count_men) {
        this.count_men = count_men;
    }

    public String getCount_women() {
        return count_women;
    }

    public void setCount_women(String count_women) {
        this.count_women = count_women;
    }

    public String getMarried_men() {
        return married_men;
    }

    public void setMarried_men(String married_men) {
        this.married_men = married_men;
    }

    public String getMarried_women() {
        return married_women;
    }

    public void setMarried_women(String married_women) {
        this.married_women = married_women;
    }

    public String getGraduate_men() {
        return graduate_men;
    }

    public void setGraduate_men(String graduate_men) {
        this.graduate_men = graduate_men;
    }

    public String getGraduate_women() {
        return graduate_women;
    }

    public void setGraduate_women(String graduate_women) {
        this.graduate_women = graduate_women;
    }

    public String getSingle_men() {
        return single_men;
    }

    public void setSingle_men(String single_men) {
        this.single_men = single_men;
    }

    public String getSingle_women() {
        return single_women;
    }

    public void setSingle_women(String single_women) {
        this.single_women = single_women;
    }

    public ArrayList<StatisticsModel> getStatistics() {
        return statistics;
    }

    public void setStatistics(ArrayList<StatisticsModel> statistics) {
        this.statistics = statistics;
    }
}
