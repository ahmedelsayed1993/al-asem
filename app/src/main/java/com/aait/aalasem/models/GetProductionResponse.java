package com.aait.aalasem.models;

public class GetProductionResponse extends BaseResponse {
    private GetProductionModel data;

    public GetProductionModel getData() {
        return data;
    }

    public void setData(GetProductionModel data) {
        this.data = data;
    }
}
