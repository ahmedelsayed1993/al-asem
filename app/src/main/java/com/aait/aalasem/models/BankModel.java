package com.aait.aalasem.models;

import java.io.Serializable;

public class BankModel implements Serializable {
    private String bank_name;
    private String bank_number;
    private String person_dontate_cost;

    public String getBank_name() {
        return bank_name;
    }

    public void setBank_name(String bank_name) {
        this.bank_name = bank_name;
    }

    public String getBank_number() {
        return bank_number;
    }

    public void setBank_number(String bank_number) {
        this.bank_number = bank_number;
    }

    public String getPerson_dontate_cost() {
        return person_dontate_cost;
    }

    public void setPerson_dontate_cost(String person_dontate_cost) {
        this.person_dontate_cost = person_dontate_cost;
    }
}
