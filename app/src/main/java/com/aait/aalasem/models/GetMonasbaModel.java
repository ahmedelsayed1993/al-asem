package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class GetMonasbaModel implements Serializable {
    private String word;
    private ArrayList<MonasbaModel> occasions;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public ArrayList<MonasbaModel> getOccasions() {
        return occasions;
    }

    public void setOccasions(ArrayList<MonasbaModel> occasions) {
        this.occasions = occasions;
    }
}
