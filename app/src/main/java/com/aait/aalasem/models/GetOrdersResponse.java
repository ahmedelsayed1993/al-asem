package com.aait.aalasem.models;

public class GetOrdersResponse extends BaseResponse {
    private GetOrderModel data;

    public GetOrderModel getData() {
        return data;
    }

    public void setData(GetOrderModel data) {
        this.data = data;
    }
}
