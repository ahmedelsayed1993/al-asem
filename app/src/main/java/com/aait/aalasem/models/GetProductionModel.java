package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class GetProductionModel implements Serializable {
    private String word;
    private ArrayList<ProductionModel> productions;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public ArrayList<ProductionModel> getProductions() {
        return productions;
    }

    public void setProductions(ArrayList<ProductionModel> productions) {
        this.productions = productions;
    }
}
