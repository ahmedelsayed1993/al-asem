package com.aait.aalasem.models;

public class SocialResponse extends BaseResponse {
    private SocialModel data;

    public SocialModel getData() {
        return data;
    }

    public void setData(SocialModel data) {
        this.data = data;
    }
}
