package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class GetOrderModel implements Serializable {
    private String word;
    private ArrayList<OrdersModel> orders;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public ArrayList<OrdersModel> getOrders() {
        return orders;
    }

    public void setOrders(ArrayList<OrdersModel> orders) {
        this.orders = orders;
    }
}
