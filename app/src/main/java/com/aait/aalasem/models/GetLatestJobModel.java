package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class GetLatestJobModel implements Serializable {
    private ArrayList<LatestJobsModel> jops;

    public ArrayList<LatestJobsModel> getJops() {
        return jops;
    }

    public void setJops(ArrayList<LatestJobsModel> jops) {
        this.jops = jops;
    }
}
