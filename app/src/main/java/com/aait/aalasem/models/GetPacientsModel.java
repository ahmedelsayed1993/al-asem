package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class GetPacientsModel implements Serializable {
    private String word;
    private ArrayList<PacientModel> patients;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public ArrayList<PacientModel> getPatients() {
        return patients;
    }

    public void setPatients(ArrayList<PacientModel> patients) {
        this.patients = patients;
    }
}
