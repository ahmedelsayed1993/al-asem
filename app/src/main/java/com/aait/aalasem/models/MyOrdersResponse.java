package com.aait.aalasem.models;

import java.util.ArrayList;

public class MyOrdersResponse extends BaseResponse {
    private ArrayList<MyOrderModel> data;

    public ArrayList<MyOrderModel> getData() {
        return data;
    }

    public void setData(ArrayList<MyOrderModel> data) {
        this.data = data;
    }
}
