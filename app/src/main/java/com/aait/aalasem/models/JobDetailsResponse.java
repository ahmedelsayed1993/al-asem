package com.aait.aalasem.models;

public class JobDetailsResponse extends BaseResponse {
    private JopsModel data;

    public JopsModel getData() {
        return data;
    }

    public void setData(JopsModel data) {
        this.data = data;
    }
}
