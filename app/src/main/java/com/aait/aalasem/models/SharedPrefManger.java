package com.aait.aalasem.models;

import android.content.Context;
import android.content.SharedPreferences;

import com.aait.aalasem.App.Constant;
import com.google.gson.Gson;

public class SharedPrefManger { Context mContext;

    SharedPreferences mSharedPreferences;

    SharedPreferences.Editor mEditor;

    public SharedPrefManger(Context mContext) {
        this.mContext = mContext;
        mSharedPreferences = mContext.getSharedPreferences(Constant.SharedPrefKey.SHARED_PREF_NAME, mContext.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public Boolean getLoginStatus() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.LOGIN_STATUS, false);
        return value;
    }

    public void setLoginStatus(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.LOGIN_STATUS, status);
        mEditor.commit();
    }

    public Boolean getNotificationStatus() {
        Boolean value = mSharedPreferences.getBoolean(Constant.SharedPrefKey.NOTIFICATION, true);
        return value;
    }

    public void setNotificationStatus(Boolean status) {
        mEditor.putBoolean(Constant.SharedPrefKey.NOTIFICATION, status);
        mEditor.commit();
    }


    public void setUserData(ActiveUserModel userModel) {
        mEditor.putString(Constant.SharedPrefKey.USER, new Gson().toJson(userModel));
        mEditor.apply();
    }

    public ActiveUserModel getUserData() {
        Gson gson = new Gson();
        return gson.fromJson(mSharedPreferences.getString(Constant.SharedPrefKey.USER, null), ActiveUserModel.class);
    }


    public void Logout() {
        mEditor.clear();
        mEditor.apply();
    }
}

