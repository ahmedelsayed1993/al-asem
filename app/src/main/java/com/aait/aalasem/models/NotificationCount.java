package com.aait.aalasem.models;

public class NotificationCount extends BaseResponse {
    private int data;

    public int getData() {
        return data;
    }

    public void setData(int data) {
        this.data = data;
    }
}
