package com.aait.aalasem.models;

import java.io.Serializable;

public class UserDataModel implements Serializable {
    private String first_name;
    private String last_name;
    private String phone;
    private String avatar;
    private String location;
    private int in_dictionary;
    private String jop;
    private String middle_name;
    private String  social_status;
    private String sons_count;
    private String educational_qualification;
    private String lat;
    private String lng;

    public String getMiddle_name() {
        return middle_name;
    }

    public void setMiddle_name(String middle_name) {
        this.middle_name = middle_name;
    }

    public String getSocial_status() {
        return social_status;
    }

    public void setSocial_status(String social_status) {
        this.social_status = social_status;
    }

    public String getSons_count() {
        return sons_count;
    }

    public void setSons_count(String sons_count) {
        this.sons_count = sons_count;
    }

    public String getEducational_qualification() {
        return educational_qualification;
    }

    public void setEducational_qualification(String educational_qualification) {
        this.educational_qualification = educational_qualification;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public int getIn_dictionary() {
        return in_dictionary;
    }

    public void setIn_dictionary(int in_dictionary) {
        this.in_dictionary = in_dictionary;
    }

    public String getJop() {
        return jop;
    }

    public void setJop(String jop) {
        this.jop = jop;
    }
}
