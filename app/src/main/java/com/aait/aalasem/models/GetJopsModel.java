package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class GetJopsModel implements Serializable {
    private String word;
    private ArrayList<JopsModel> jops;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public ArrayList<JopsModel> getJops() {
        return jops;
    }

    public void setJops(ArrayList<JopsModel> jops) {
        this.jops = jops;
    }
}
