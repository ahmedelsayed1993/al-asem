package com.aait.aalasem.models;

public class ForgetResponse extends BaseResponse {
    private ForgetPassModel data;

    public ForgetPassModel getData() {
        return data;
    }

    public void setData(ForgetPassModel data) {
        this.data = data;
    }
}
