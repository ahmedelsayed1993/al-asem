package com.aait.aalasem.models;

public class OrderDetailsResponse extends BaseResponse {
    private OrdersModel data;

    public OrdersModel getData() {
        return data;
    }

    public void setData(OrdersModel data) {
        this.data = data;
    }
}
