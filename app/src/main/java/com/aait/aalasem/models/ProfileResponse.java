package com.aait.aalasem.models;

public class ProfileResponse extends BaseResponse {
    private UserDataModel data;

    public UserDataModel getData() {
        return data;
    }

    public void setData(UserDataModel data) {
        this.data = data;
    }
}
