package com.aait.aalasem.models;

import java.io.Serializable;
import java.util.ArrayList;

public class GetJobResquestModel implements Serializable {
    private String word;
    private ArrayList<LatestJobsModel> jops;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public ArrayList<LatestJobsModel> getJops() {
        return jops;
    }

    public void setJops(ArrayList<LatestJobsModel> jops) {
        this.jops = jops;
    }
}
