package com.aait.aalasem.models;

import java.io.Serializable;

public class ActiveUserModel implements Serializable {
    private UserDataModel user;
    private String  token;

    public UserDataModel getUser() {
        return user;
    }

    public void setUser(UserDataModel user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
