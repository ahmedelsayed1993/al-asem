package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.JopsModel;
import com.aait.aalasem.models.NewsModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class JobsAdapter extends ParentRecyclerAdapter<JopsModel> {
    public JobsAdapter(Context context, List<JopsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        JobsAdapter.ViewHolder holder = new JobsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final JobsAdapter.ViewHolder viewHolder = (JobsAdapter.ViewHolder) holder;
        JopsModel jopsModel = data.get(position);
        Glide.with(mcontext).load(jopsModel.getFile()).asBitmap().placeholder(R.mipmap.mlogo).into(viewHolder.image);
        viewHolder.title.setText(jopsModel.getTitle());
        viewHolder.name.setText(jopsModel.getUser());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.lay_image)
        ImageView image;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.name)
        TextView name;





        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
