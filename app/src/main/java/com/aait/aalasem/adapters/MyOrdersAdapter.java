package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.MyOrderModel;
import com.aait.aalasem.models.NewsModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class MyOrdersAdapter extends ParentRecyclerAdapter<MyOrderModel> {
    public MyOrdersAdapter(Context context, List<MyOrderModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        MyOrdersAdapter.ViewHolder holder = new MyOrdersAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final MyOrdersAdapter.ViewHolder viewHolder = (MyOrdersAdapter.ViewHolder) holder;
        MyOrderModel myOrderModel = data.get(position);
        Glide.with(mcontext).load(myOrderModel.getAvatar()).asBitmap().placeholder(R.mipmap.mlogo).into(viewHolder.image);
        viewHolder.body.setText(myOrderModel.getDetails());
        viewHolder.date.setText(myOrderModel.getCreated_at());
        if (myOrderModel.getStatus().equals("waiting")){
            viewHolder.status.setText(mcontext.getResources().getString(R.string.waiting));
            viewHolder.status.setBackgroundColor(mcontext.getResources().getColor(R.color.colorYellow));
        }else if (myOrderModel.getStatus().equals("active")){
            viewHolder.status.setText(mcontext.getResources().getString(R.string.active));
            viewHolder.status.setBackgroundColor(mcontext.getResources().getColor(R.color.colorGreen));
        }else {
            viewHolder.status.setText(mcontext.getResources().getString(R.string.deactive));
            viewHolder.status.setBackgroundColor(mcontext.getResources().getColor(R.color.colorRed));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.status)
        TextView status;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.body)
        TextView body;
        @BindView(R.id.ci_image)
        CircleImageView image;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
