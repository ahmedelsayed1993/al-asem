package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.DeadModel;
import com.aait.aalasem.models.NewsModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class NewsAdapter extends ParentRecyclerAdapter<NewsModel> {
    public NewsAdapter(Context context, List<NewsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        NewsAdapter.ViewHolder holder = new NewsAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final NewsAdapter.ViewHolder viewHolder = (NewsAdapter.ViewHolder) holder;
        NewsModel newsModel = data.get(position);
        Glide.with(mcontext).load(newsModel.getImage()).asBitmap().placeholder(R.mipmap.mlogo).into(viewHolder.image);
        viewHolder.title.setText(newsModel.getTitle());
        viewHolder.bogy.setText(newsModel.getBody());
        viewHolder.user_data.setText(newsModel.getUser()+"  " +newsModel.getCreated_at());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.user_data)
        TextView user_data;
        @BindView(R.id.body)
        TextView bogy;
        @BindView(R.id.new_image)
        ImageView image;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
