package com.aait.aalasem.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.NavigationModel;

import java.util.List;

import butterknife.BindView;

public class NavigationDrawerAdapter extends ParentRecyclerAdapter<NavigationModel> {

    public NavigationDrawerAdapter(final Context context, final List<NavigationModel> data, final int layoutId) {
        super(context, data, layoutId);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(final ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ViewHolder holder = new ViewHolder(itemView);
        holder.setClickableRootView(holder.layNav);
        holder.setOnItemClickListener(itemClickListener);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ParentRecyclerViewHolder holder, final int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        NavigationModel navigationModel = data.get(position);
        viewHolder.tvNavName.setText(navigationModel.getNavTitle());
        viewHolder.iv_menu_item_icon.setImageResource(navigationModel.getNatIcon());
    }


    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.tv_nav_name)
        TextView tvNavName;

        @BindView(R.id.iv_menu_item_icon)
        ImageView iv_menu_item_icon;

        @BindView(R.id.lay_nav)
        RelativeLayout layNav;

        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}


