package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.NewsModel;
import com.aait.aalasem.models.PacientModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class PatientAdapter extends ParentRecyclerAdapter<PacientModel> {
    public PatientAdapter(Context context, List<PacientModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        PatientAdapter.ViewHolder holder = new PatientAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final PatientAdapter.ViewHolder viewHolder = (PatientAdapter.ViewHolder) holder;
        PacientModel pacientModel = data.get(position);
        Glide.with(mcontext).load(pacientModel.getImage()).asBitmap().placeholder(R.mipmap.mlogo).into(viewHolder.image);
        viewHolder.name.setText(pacientModel.getName());
        viewHolder.bogy.setText(pacientModel.getBody());
        viewHolder.user_data.setText(pacientModel.getCreated_at());
        viewHolder.extra.setText(pacientModel.getExtra());

        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.user_data)
        TextView user_data;
        @BindView(R.id.body)
        TextView bogy;
        @BindView(R.id.extra)
        TextView extra;
        @BindView(R.id.new_image)
        ImageView image;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
