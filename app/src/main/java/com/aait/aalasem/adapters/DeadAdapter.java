package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.DeadModel;
import com.aait.aalasem.models.StatisticsModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class DeadAdapter extends ParentRecyclerAdapter<DeadModel> {
    public DeadAdapter(Context context, List<DeadModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        DeadAdapter.ViewHolder holder = new DeadAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final DeadAdapter.ViewHolder viewHolder = (DeadAdapter.ViewHolder) holder;
        DeadModel deadModel = data.get(position);
        Glide.with(mcontext).load(deadModel.getImage()).asBitmap().placeholder(R.mipmap.mlogo).into(viewHolder.image);
        viewHolder.date.setText(deadModel.getDate());
        viewHolder.name.setText(deadModel.getName());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.dead_date)
        TextView date;
        @BindView(R.id.dead_name)
                TextView name;
        @BindView(R.id.dead_image)
        ImageView image;



        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
