package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.JopsModel;
import com.aait.aalasem.models.MonasbaModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class MonasbatAdapter extends ParentRecyclerAdapter<MonasbaModel> {
    public MonasbatAdapter(Context context, List<MonasbaModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        MonasbatAdapter.ViewHolder holder = new MonasbatAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final MonasbatAdapter.ViewHolder viewHolder = (MonasbatAdapter.ViewHolder) holder;
        MonasbaModel monasbaModel = data.get(position);
        Glide.with(mcontext).load(monasbaModel.getImage()).asBitmap().placeholder(R.mipmap.mlogo).into(viewHolder.image);
        viewHolder.name.setText(monasbaModel.getTitle());
        viewHolder.user_data.setText(monasbaModel.getCreated_at());
        viewHolder.body.setText(monasbaModel.getBody());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.new_image)
        ImageView image;
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.user_data)
        TextView user_data;
        @BindView(R.id.body)
        TextView body;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
