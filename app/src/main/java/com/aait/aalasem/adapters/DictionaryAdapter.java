package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.DeadModel;
import com.aait.aalasem.models.UserDataModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class DictionaryAdapter extends ParentRecyclerAdapter<UserDataModel> {
    public DictionaryAdapter(Context context, List<UserDataModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        DictionaryAdapter.ViewHolder holder = new DictionaryAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final DictionaryAdapter.ViewHolder viewHolder = (DictionaryAdapter.ViewHolder) holder;
        UserDataModel deadModel = data.get(position);
        Glide.with(mcontext).load(deadModel.getAvatar()).asBitmap().placeholder(R.mipmap.mlogo).into(viewHolder.image);
        viewHolder.phone.setText(deadModel.getPhone());
        viewHolder.name.setText(deadModel.getFirst_name()+" "+deadModel.getLast_name());
        viewHolder.address.setText(deadModel.getLocation());
        viewHolder.jop.setText(deadModel.getJop());
        viewHolder.location_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });
        viewHolder.details_lay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.phone)
        TextView phone;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.address)
        TextView address;
        @BindView(R.id.jop)
        TextView jop;
        @BindView(R.id.location_lay)
        LinearLayout location_lay;
        @BindView(R.id.details_lay)
                LinearLayout details_lay;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
