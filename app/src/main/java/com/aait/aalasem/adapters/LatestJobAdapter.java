package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.JopsModel;
import com.aait.aalasem.models.LatestJobsModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class LatestJobAdapter extends ParentRecyclerAdapter<LatestJobsModel> {
    public LatestJobAdapter(Context context, List<LatestJobsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        LatestJobAdapter.ViewHolder holder = new LatestJobAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final LatestJobAdapter.ViewHolder viewHolder = (LatestJobAdapter.ViewHolder) holder;
        LatestJobsModel latestJobsModel = data.get(position);
        viewHolder.body.setText(latestJobsModel.getBody());
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.body)
        TextView body;






        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
