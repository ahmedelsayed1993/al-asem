package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.NavigationModel;
import com.aait.aalasem.models.StatisticsModel;

import java.util.List;

import butterknife.BindView;

public class StatisticAdapter extends ParentRecyclerAdapter<StatisticsModel> {
    public StatisticAdapter(Context context, List<StatisticsModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        StatisticAdapter.ViewHolder holder = new StatisticAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, int position) {
        final StatisticAdapter.ViewHolder viewHolder = (StatisticAdapter.ViewHolder) holder;
        StatisticsModel navigationModel = data.get(position);
        viewHolder.sta.setText(navigationModel.getKey()+":"+navigationModel.getValue());

    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.sta)
        TextView sta;



        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
