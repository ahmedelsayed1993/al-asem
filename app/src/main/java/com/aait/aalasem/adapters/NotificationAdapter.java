package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.DeadModel;
import com.aait.aalasem.models.NotificationModel;
import com.aait.aalasem.models.SharedPrefManger;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import de.hdodenhof.circleimageview.CircleImageView;

public class NotificationAdapter extends ParentRecyclerAdapter<NotificationModel> {
    SharedPrefManger sharedPrefManger = new SharedPrefManger(mcontext);
    public NotificationAdapter(Context context, List<NotificationModel> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        NotificationAdapter.ViewHolder holder = new NotificationAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final NotificationAdapter.ViewHolder viewHolder = (NotificationAdapter.ViewHolder) holder;
        NotificationModel notificationModel = data.get(position);
        Glide.with(mcontext).load(sharedPrefManger.getUserData().getUser().getAvatar()).asBitmap().placeholder(R.mipmap.mlogo).into(viewHolder.image);
        viewHolder.date.setText(notificationModel.getCreated_at());
        viewHolder.notify_tittle.setText(notificationModel.getMessage());
        viewHolder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {

        @BindView(R.id.notify_tittle)
        TextView notify_tittle;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.image)
        CircleImageView image;
        @BindView(R.id.delete)
        ImageView delete;



        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
