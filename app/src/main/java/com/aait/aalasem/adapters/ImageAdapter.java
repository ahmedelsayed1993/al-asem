package com.aait.aalasem.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentRecyclerAdapter;
import com.aait.aalasem.base.ParentRecyclerViewHolder;
import com.aait.aalasem.models.NewsModel;
import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;

public class ImageAdapter extends ParentRecyclerAdapter<String> {
    public ImageAdapter(Context context, List<String> data, int layoutId) {
        super(context, data, layoutId);
    }

    @NonNull
    @Override
    public ParentRecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mcontext).inflate(layoutId, parent, false);
        ImageAdapter.ViewHolder holder = new ImageAdapter.ViewHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ParentRecyclerViewHolder holder, final int position) {
        final ImageAdapter.ViewHolder viewHolder = (ImageAdapter.ViewHolder) holder;
        String image = data.get(position);
        Glide.with(mcontext).load(image).asBitmap().placeholder(R.mipmap.mlogo).into(viewHolder.image);
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                itemClickListener.onItemClick(v,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    public class ViewHolder extends ParentRecyclerViewHolder {


        @BindView(R.id.image)
        ImageView image;




        ViewHolder(View itemView) {
            super(itemView);
        }

    }
}
