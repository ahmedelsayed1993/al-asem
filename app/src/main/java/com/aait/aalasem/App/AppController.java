package com.aait.aalasem.App;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.os.Build;

//import com.tsengvn.typekit.Typekit;

public class AppController extends Application {
    public static final String TAG = AppController.class.getSimpleName();

    private static AppController mInstance;

  //  public static GlobalPreferences globalPreferences;

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
       // globalPreferences = new GlobalPreferences(this);
       // Util.setConfig("ar",getApplicationContext());
//        Typekit.getInstance()
//                .addNormal(Typekit.createFromAsset(this, "fonts/" + "JF-Flat-regular.ttf"))
//                .addBold(Typekit.createFromAsset(this, "fonts/" + "JF-Flat-medium.ttf"));
        //updateAndroidSecurityProvider();

    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //        Util.setConfig("ar",getApplicationContext());
        }
    }



    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);

    }
}
