package com.aait.aalasem.Views;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.models.BaseResponse;
import com.aait.aalasem.models.SharedPrefManger;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddOrderDialog extends Dialog {
    Context mContext;
    SharedPrefManger sharedPrefManger;
    @BindView(R.id.ed_name)
    EditText ed_name;
    @BindView(R.id.ed_Phone)
    EditText ed_Phone;
    @BindView(R.id.ed_details)
    EditText ed_details;
    public AddOrderDialog(@NonNull Context context) {
        super(context);
        this.mContext = context;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_new_order);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        getWindow().setGravity(Gravity.CENTER);
        setCancelable(true);
        ButterKnife.bind(this);
        sharedPrefManger = new SharedPrefManger(mContext);
        initializeComponents();
    }
    private void initializeComponents() {

    }
    @OnClick(R.id.btn_send)
    void onSendClick(){
        if (ed_name.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,mContext.getString(R.string.please_enter)+" "+mContext.getString(R.string.name));
        }else if (ed_Phone.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,mContext.getString(R.string.please_enter)+" "+mContext.getString(R.string.phone_number));
        }else if (ed_details.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,mContext.getString(R.string.please_enter)+" "+mContext.getString(R.string.write_order_details));
        }else {
            addOrder();
        }
    }
    private void addOrder(){
        RetroWeb.getClient().create(ServiceApi.class).addOrder(Urls.Bearer +sharedPrefManger.getUserData().getToken(),ed_name.getText().toString(),ed_Phone.getText().toString(),ed_details.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        CommonUtil.makeToast(mContext,mContext.getString(R.string.waiting_admin));
                        dismiss();
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                dismiss();

            }
        });
    }
}
