package com.aait.aalasem.activities;

import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.JobDetailsResponse;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class JobDetailsActivity extends ParentActivity {
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.civ_image)
    CircleImageView civ_image;
    @BindView(R.id.name)
    TextView name;
    @BindView(R.id.titile)
    TextView titile;
    @BindView(R.id.details)
    TextView details;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    String id;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.job_chance));
        id = getIntent().getStringExtra("id");
        getJob();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_job_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getJob(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getJob(Integer.parseInt(id)).enqueue(new Callback<JobDetailsResponse>() {
            @Override
            public void onResponse(Call<JobDetailsResponse> call, Response<JobDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        titile.setText(response.body().getData().getTitle());
                        name.setText(response.body().getData().getUser());
                        details.setText(response.body().getData().getBody());
                        Glide.with(mContext).load(response.body().getData().getFile()).asBitmap().placeholder(R.mipmap.mlogo).into(civ_image);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<JobDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
