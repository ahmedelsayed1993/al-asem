package com.aait.aalasem.activities;

import android.content.Intent;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.OrderDetailsResponse;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class OrderDetailsActivity extends ParentActivity {
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.title)
            TextView titile;
    @BindView(R.id.details)
            TextView details;
    String id;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.kabela_box));
        id = getIntent().getStringExtra("id");
        getOrder();

    }
    @OnClick(R.id.pay)
    void onPayClick(){
        startActivity(new Intent(mContext,TransferActivity.class));
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_order_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getOrder(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getOrder(Integer.parseInt(id)).enqueue(new Callback<OrderDetailsResponse>() {
            @Override
            public void onResponse(Call<OrderDetailsResponse> call, Response<OrderDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        titile.setText(response.body().getData().getName());
                        details.setText(response.body().getData().getDetails());
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        titile.setText("");
                        details.setText("");
                    }
                }
            }

            @Override
            public void onFailure(Call<OrderDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
