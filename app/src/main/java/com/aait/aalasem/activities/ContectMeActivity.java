package com.aait.aalasem.activities;

import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.BaseResponse;
import com.aait.aalasem.models.ConditionsResponse;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContectMeActivity extends ParentActivity {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        onBackPressed();
    }
    @BindView(R.id.text2)
    TextView text2;
    @BindView(R.id.ed_name)
    EditText ed_name;
    @BindView(R.id.ed_mailOrPhone)
    EditText ed_mailOrPhone;
    @BindView(R.id.ed_address)
    EditText ed_address;
    @BindView(R.id.ed_details)
    EditText ed_details;


    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.contact_us));
        getContact();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_contact_me;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_send)
    void onSend(){
        if (ed_name.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.name));
        }else if (ed_mailOrPhone.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.mail_phone));
        }else if (ed_address.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.address));
        }else if (ed_details.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.detail));
        }else {
            contact();
        }
    }
    private void getContact() {
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getContact().enqueue(new Callback<ConditionsResponse>() {
            @Override
            public void onResponse(Call<ConditionsResponse> call, Response<ConditionsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getStatus().equals("1")) {
                        text2.setText(response.body().getData());
                    } else {
                        CommonUtil.makeToast(mContext, response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ConditionsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext, t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void contact(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).contact(Urls.Bearer+mSharedPrefManager.getUserData().getToken(),ed_name.getText().toString()
        ,ed_mailOrPhone.getText().toString(),ed_address.getText().toString(),ed_details.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        ed_address.setText("");
                        ed_details.setText("");
                        ed_mailOrPhone.setText("");
                        ed_name.setText("");
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
