package com.aait.aalasem.activities;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.adapters.StatisticAdapter;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.StaticticsResponse;
import com.aait.aalasem.models.StatisticsModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class StatisticsActivity extends ParentActivity {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        startActivity(new Intent(mContext,MainActivity.class));
    }
    @BindView(R.id.total_count)
    TextView total_count;
    @BindView(R.id.female_count)
    TextView female_count;
    @BindView(R.id.male_count)
    TextView male_count;
    @BindView(R.id.male_graduate_count)
    TextView male_graduate_count;
    @BindView(R.id.female_graduate_count)
    TextView female_graduate_count;
    @BindView(R.id.male_married_count)
    TextView male_married_count;
    @BindView(R.id.female_married_count)
    TextView female_married_count;
    @BindView(R.id.male_single_count)
    TextView male_single_count;
    @BindView(R.id.female_single_count)
    TextView female_single_count;
    @BindView(R.id.other_recycler)
    RecyclerView other_recycler;
    StatisticAdapter statisticAdapter;
    GridLayoutManager gridLayoutManager;
    ArrayList<StatisticsModel> statisticsModels = new ArrayList<>();

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.statistics));
        gridLayoutManager = new GridLayoutManager(mContext,3);
        statisticAdapter = new StatisticAdapter(mContext,statisticsModels,R.layout.recycler_statstics);
        other_recycler.setLayoutManager(gridLayoutManager);
        other_recycler.setAdapter(statisticAdapter);
        getStatistic();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_kabela_statistics;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getStatistic(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getStatistic().enqueue(new Callback<StaticticsResponse>() {
            @Override
            public void onResponse(Call<StaticticsResponse> call, Response<StaticticsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        total_count.setText(getString(R.string.total_count)+":"+ response.body().getData().getCount_people_in_tribe()+getString(R.string.person));
                        male_count.setText(getString(R.string.mans)+response.body().getData().getCount_men()+getString(R.string.man));
                        female_count.setText(getString(R.string.women)+response.body().getData().getCount_women()+getString(R.string.woman));
                        male_married_count.setText(getString(R.string.mans)+response.body().getData().getMarried_men()+getString(R.string.man));
                        female_married_count.setText(getString(R.string.women)+response.body().getData().getMarried_women()+getString(R.string.woman));
                        male_single_count.setText(getString(R.string.mans)+response.body().getData().getSingle_men()+getString(R.string.man));
                        female_single_count.setText(getString(R.string.women)+response.body().getData().getSingle_women()+getString(R.string.woman));
                        male_graduate_count.setText(getString(R.string.mans)+response.body().getData().getGraduate_men()+getString(R.string.man));
                        female_graduate_count.setText(getString(R.string.women)+response.body().getData().getGraduate_women()+getString(R.string.woman));
                        statisticsModels = response.body().getData().getStatistics();
                        statisticAdapter.updateAll(response.body().getData().getStatistics());

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<StaticticsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
