package com.aait.aalasem.activities;


import android.content.Intent;

import android.widget.ImageView;

import android.widget.TextView;



import com.aait.aalasem.R;

import com.aait.aalasem.base.ParentActivity;


import butterknife.BindView;
import butterknife.OnClick;


public class EmploymentActivity extends ParentActivity {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;

    @OnClick(R.id.act_back)
    void onActBack() {
        startActivity(new Intent(mContext,MainActivity.class));
    }




    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.employment_corner));





    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_employment;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @OnClick(R.id.iv_chance)
    void onChanceClick() {
        startActivity(new Intent(mContext, JobChanceActivity.class));
    }

    @OnClick(R.id.iv_want)
    void onWantClick() {
        startActivity(new Intent(mContext, EmployedPeopleActivity.class));
    }


}

