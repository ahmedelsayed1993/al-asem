package com.aait.aalasem.activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.BaseResponse;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WantJobActivity extends ParentActivity {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        onBackPressed();
    }
    @BindView(R.id.file)
    TextView file;
    @BindView(R.id.ed_address)
    EditText ed_address;
    @BindView(R.id.ed_details)
    EditText ed_details;
    private String type = null;
    private static final int PICK_FILE_REQUEST = 1;
    private static final String TAG = WantJobActivity.class.getSimpleName();
    private String selectedFilePath;
    ProgressDialog dialog;

    private static final int REQUEST_CODE = 4;
    private static final int REQUEST_CODE_MATISSE = 5;

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.want_job));


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_want_job;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.add_image)
    void onAdd(){
        getFile();

    }
    private void getFile() {
        if (ActivityCompat.checkSelfPermission(WantJobActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE);
        } else {
            attach();
        }
    }
    private void attach() {
        Intent intent = new Intent();
        intent.setType("application/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent,
                "Select file to upload "), REQUEST_CODE_MATISSE);

    }
    @OnClick(R.id.btn_send)
    void onSendClick(){
        if (ed_address.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.sender_mail_phone));
        }else if (ed_details.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.request_details));
        }else if (selectedFilePath == null){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.add_files));
        }else {
            AddOrder(selectedFilePath);
        }
    }
    private void showFileChooser() {
        Intent intent = new Intent();
        //sets the select file to all types of files
        intent.setType("*/*");
        //allows to select data and return it
        intent.setAction(Intent.ACTION_GET_CONTENT);
        //starts new activity to select file and return data
        startActivityForResult(Intent.createChooser(intent,"Choose File to Upload.."),PICK_FILE_REQUEST);
    }
    private void AddOrder(String path){
        showProgressDialog(getString(R.string.please_wait));
        File file = new File(path);
        final RequestBody requestBody = RequestBody.create(MediaType.parse("file/*"),file);
        MultipartBody.Part filepart = MultipartBody.Part.createFormData("file",file.getName(),requestBody);
        RetroWeb.getClient().create(ServiceApi.class).addJobRequest(Urls.Bearer+mSharedPrefManager.getUserData().getToken(),ed_address.getText().toString(),ed_details.getText().toString(),filepart).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_MATISSE) {
            if (resultCode == RESULT_OK) {
                Uri uri = data.getData();
                Log.e("path", uri.toString());
                String path = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    path = CommonUtil.getPath(this, uri);
                    Log.e("path", path);

                    type = getContentResolver().getType(uri);
                    Log.e("type",type);
                    if ((type.startsWith("application"))){
                        selectedFilePath = CommonUtil.getPath(WantJobActivity.this,uri);
                        file.setText(selectedFilePath);

                    }else if (type.startsWith("text")){
                        selectedFilePath = CommonUtil.getPath(WantJobActivity.this,uri);
                        file.setText(selectedFilePath);
                    }else if (!(type.startsWith("image")&& type.startsWith("video")&& type.startsWith("audio"))){
                        CommonUtil.makeToast(mContext,getString(R.string.choose_only_files));
                    }
                }

            }

        }
    }
//    public int uploadFile(final String selectedFilePath){
//
//        int serverResponseCode = 0;
//
//        HttpURLConnection connection;
//        DataOutputStream dataOutputStream;
//        String lineEnd = "\r\n";
//        String twoHyphens = "--";
//        String boundary = "*****";
//
//
//        int bytesRead,bytesAvailable,bufferSize;
//        byte[] buffer;
//        int maxBufferSize = 1 * 1024 * 1024;
//        File selectedFile = new File(selectedFilePath);
//
//
//        String[] parts = selectedFilePath.split("/");
//        final String fileName = parts[parts.length-1];
//
//        if (!selectedFile.isFile()){
//            dialog.dismiss();
//
//            runOnUiThread(new Runnable() {
//                @Override
//                public void run() {
//                    file.setText("Source File Doesn't Exist: " + selectedFilePath);
//                }
//            });
//            return 0;
//        }else{
//            try{
//                FileInputStream fileInputStream = new FileInputStream(selectedFile);
//                URL url = new URL(Urls.baseUrl);
//                connection = (HttpURLConnection) url.openConnection();
//                connection.setDoInput(true);//Allow Inputs
//                connection.setDoOutput(true);//Allow Outputs
//                connection.setUseCaches(false);//Don't use a cached Copy
//                connection.setRequestMethod("POST");
//                connection.setRequestProperty("Connection", "Keep-Alive");
//                connection.setRequestProperty("ENCTYPE", "multipart/form-data");
//                connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
//                connection.setRequestProperty("uploaded_file",selectedFilePath);
//
//                //creating new dataoutputstream
//                dataOutputStream = new DataOutputStream(connection.getOutputStream());
//
//                //writing bytes to data outputstream
//                dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
//                dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
//                        + selectedFilePath + "\"" + lineEnd);
//
//                dataOutputStream.writeBytes(lineEnd);
//
//                //returns no. of bytes present in fileInputStream
//                bytesAvailable = fileInputStream.available();
//                //selecting the buffer size as minimum of available bytes or 1 MB
//                bufferSize = Math.min(bytesAvailable,maxBufferSize);
//                //setting the buffer as byte array of size of bufferSize
//                buffer = new byte[bufferSize];
//
//                //reads bytes from FileInputStream(from 0th index of buffer to buffersize)
//                bytesRead = fileInputStream.read(buffer,0,bufferSize);
//
//                //loop repeats till bytesRead = -1, i.e., no bytes are left to read
//                while (bytesRead > 0){
//                    //write the bytes read from inputstream
//                    dataOutputStream.write(buffer,0,bufferSize);
//                    bytesAvailable = fileInputStream.available();
//                    bufferSize = Math.min(bytesAvailable,maxBufferSize);
//                    bytesRead = fileInputStream.read(buffer,0,bufferSize);
//                }
//
//                dataOutputStream.writeBytes(lineEnd);
//                dataOutputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
//
//                serverResponseCode = connection.getResponseCode();
//                String serverResponseMessage = connection.getResponseMessage();
//
//                Log.i(TAG, "Server Response is: " + serverResponseMessage + ": " + serverResponseCode);
//
//                //response code of 200 indicates the server status OK
//                if(serverResponseCode == 200){
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            file.setText("File Upload completed.\n\n You can see the uploaded file here: \n\n" + "http://coderefer.com/extras/uploads/"+ fileName);
//                        }
//                    });
//                }
//
//                //closing the input and output streams
//                fileInputStream.close();
//                dataOutputStream.flush();
//                dataOutputStream.close();
//
//
//
//            } catch (FileNotFoundException e) {
//                e.printStackTrace();
//                runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        Toast.makeText(WantJobActivity.this,"File Not Found",Toast.LENGTH_SHORT).show();
//                    }
//                });
//            } catch (MalformedURLException e) {
//                e.printStackTrace();
//                Toast.makeText(WantJobActivity.this, "URL error!", Toast.LENGTH_SHORT).show();
//
//            } catch (IOException e) {
//                e.printStackTrace();
//                Toast.makeText(WantJobActivity.this, "Cannot Read/Write File!", Toast.LENGTH_SHORT).show();
//            }
//            dialog.dismiss();
//            return serverResponseCode;
//        }
//
//    }
//    private void getFile() {
//        if (ActivityCompat.checkSelfPermission(WantJobActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE)
//                != PackageManager.PERMISSION_GRANTED) {
//            ActivityCompat.requestPermissions(this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_CODE);
//        } else {
//            attach();
//        }
//    }
//    private void attach() {
//        Intent intent = new Intent();
//        intent.setType("file/*");
//        intent.setAction(Intent.ACTION_GET_CONTENT);
//        startActivityForResult(Intent.createChooser(intent,
//                "Select file to upload "), REQUEST_CODE_MATISSE);
//
//    }
}
