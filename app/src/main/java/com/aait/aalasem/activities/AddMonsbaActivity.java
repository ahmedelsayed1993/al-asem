package com.aait.aalasem.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.App.Constant;
import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.Utils.PermissionUtils;
import com.aait.aalasem.Utils.ProgressRequestBody;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.BaseResponse;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.aalasem.App.Constant.RequestPermission.REQUEST_IMAGES;

public class AddMonsbaActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks{
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        onBackPressed();
    }
    @BindView(R.id.ed_location)
            TextView ed_location;
    @BindView(R.id.ed_address)
    EditText ed_address;
    @BindView(R.id.ed_details)
            EditText ed_details;
    @BindView(R.id.lay_image)
            ImageView lay_image;
    @BindView(R.id.add_image)
            ImageView add_image;
    String ImageBasePath = null;
    ArrayList<Uri> ImageList = new ArrayList<>();

    String mAdresse, mLang, mLat = null;

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.kabela_monasba));


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_monasba;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.location)
    void onMapClick(){
        DetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }
    @OnClick(R.id.btn_send)
    void onSend(){
        if (ed_address.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.monasba_address));
        }else if (ed_details.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.monasba_details));
        }else if (!ed_location.getText().toString().equals(getString(R.string.location_detected))){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.detect_location));
        }else {
            if (ImageBasePath!=null) {
                addMonasba(ImageBasePath);
            }else if (ImageBasePath==null){
                addMonasba();
            }
        }
    }
    @OnClick(R.id.add_image)
    void onAdd(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;

                        ImageBasePath = ImageList.get(0).getPath();
                        lay_image.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }
    private void addMonasba(String PathFromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, AddMonsbaActivity.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).addMonasba(Urls.Bearer+mSharedPrefManager.getUserData().getToken(),ed_address.getText().toString(),ed_details.getText().toString(),mLat,mLang,filePart).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        CommonUtil.makeToast(mContext,getString(R.string.waiting_admin));
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void addMonasba(){
        showProgressDialog(getString(R.string.please_wait));

        RetroWeb.getClient().create(ServiceApi.class).AddMonasba(Urls.Bearer+mSharedPrefManager.getUserData().getToken(),ed_address.getText().toString(),ed_details.getText().toString(),mLat,mLang).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        CommonUtil.makeToast(mContext,getString(R.string.waiting_admin));
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mLang = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLang + " Address : " + mAdresse);
                    ed_location.setText(getString(R.string.location_detected));
                }
            }
        }
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
