package com.aait.aalasem.activities;

import android.content.Intent;
import android.widget.EditText;

import com.aait.aalasem.Fcm.MyFirebaseInstanceIDService;
import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.Login;
import com.aait.aalasem.models.LoginResponse;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends ParentActivity {
    @BindView(R.id.ed_phone)
    EditText phone;
    @BindView(R.id.ed_password)
    EditText password;
    @Override
    protected void initializeComponents() {

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_login;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_register)
    void onRegisterClick(){
        startActivity(new Intent(mContext,RegisterActivity.class));
    }
    @OnClick(R.id.txt_forgot_password)
    void onForgotPassClick(){
        startActivity(new Intent(mContext,ForgotPasswordActivity.class));
    }
    @OnClick(R.id.btn_login)
    void onLoginClick(){
        if (validate()){
            Login();
        }
    }
    boolean validate(){
        if (phone.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.phone_number));
            return false;
        }else if (password.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.password));
            return false;
        }else {
            return true;
        }
    }
    private void Login(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).login(phone.getText().toString(),password.getText().toString(),"android", MyFirebaseInstanceIDService.getToken(mContext)).enqueue(new Callback<Login>() {
            @Override
            public void onResponse(Call<Login> call, Response<Login> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        mSharedPrefManager.setLoginStatus(true);
                        mSharedPrefManager.setUserData(response.body().getData());
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else if (response.body().getStatus().equals("2")){
                        Intent intent = new Intent(mContext,ActivateCodeActivity.class);
                        intent.putExtra("id",response.body().getVerifying_code().getId()+"");
                        startActivity(intent);
                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<Login> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
