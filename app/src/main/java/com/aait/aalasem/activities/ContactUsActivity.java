package com.aait.aalasem.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.Views.AddOrderDialog;
import com.aait.aalasem.adapters.MyOrdersAdapter;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.ConditionsResponse;
import com.aait.aalasem.models.MyOrderModel;
import com.aait.aalasem.models.MyOrdersResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ContactUsActivity extends ParentActivity {
    AddOrderDialog addOrderDialog;
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        onBackPressed();
    }
    @BindView(R.id.text1)
    TextView text1;
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    MyOrdersAdapter myOrdersAdapter;
    LinearLayoutManager linearLayoutManager;
    ArrayList<MyOrderModel> myOrderModels = new ArrayList<>();

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.contact_us));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        myOrdersAdapter = new MyOrdersAdapter(mContext,myOrderModels,R.layout.recycler_my_orders);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(myOrdersAdapter);
        getcontent();
        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getContact();
            }
        });
        getContact();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_contact_us;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.iv_contact_us)
    void onCOntactUsClick(){
        startActivity(new Intent(mContext,ContectMeActivity.class));
    }
    @OnClick(R.id.iv_my_orders)
    void onMyOrdersClick(){
        startActivity(new Intent(mContext,MyOrdersActivity.class));
    }
    @OnClick(R.id.btn_add_new_request)
    void onAddOrderClick(){
        addOrderDialog = new AddOrderDialog(mContext);
        addOrderDialog.show();

    }
    private void getcontent(){
        RetroWeb.getClient().create(ServiceApi.class).getContact().enqueue(new Callback<ConditionsResponse>() {
            @Override
            public void onResponse(Call<ConditionsResponse> call, Response<ConditionsResponse> response) {

                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        text1.setText(response.body().getData());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ConditionsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();


            }
        });
    }
    private void getContact(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getMyOrders(Urls.Bearer +mSharedPrefManager.getUserData().getToken()).enqueue(new Callback<MyOrdersResponse>() {
            @Override
            public void onResponse(Call<MyOrdersResponse> call, Response<MyOrdersResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                        }else {
                            myOrdersAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<MyOrdersResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);


            }
        });
    }

}
