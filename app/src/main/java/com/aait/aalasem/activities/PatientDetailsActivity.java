package com.aait.aalasem.activities;

import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.PatientDatailsResponse;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PatientDetailsActivity extends ParentActivity {
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.cir_image)
    CircleImageView cir_image;
    @BindView(R.id.title)
            TextView title;
    @BindView(R.id.date)
            TextView date;
    @BindView(R.id.body)
            TextView body;

    String id;
    @Override
    protected void initializeComponents() {
        id = getIntent().getStringExtra("id");
        getPatient();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.patient_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getPatient(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getPatient(Integer.parseInt(id)).enqueue(new Callback<PatientDatailsResponse>() {
            @Override
            public void onResponse(Call<PatientDatailsResponse> call, Response<PatientDatailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        act_title.setText(response.body().getData().getName());
                        Glide.with(mContext).load(response.body().getData().getImage()).asBitmap().placeholder(R.mipmap.mlogo).into(cir_image);
                        body.setText(response.body().getData().getBody());
                        date.setText(response.body().getData().getCreated_at());
                        title.setText(response.body().getData().getName());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<PatientDatailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
