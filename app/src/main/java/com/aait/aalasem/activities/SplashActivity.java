package com.aait.aalasem.activities;



import android.content.Intent;
import android.os.Handler;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentActivity;

import butterknife.BindView;

public class SplashActivity extends ParentActivity {
    private int s=1;
    @BindView(R.id.image)
    ImageView image;


    @Override
    protected void initializeComponents() {
        RotateAnimation anim = new RotateAnimation(0f, 360f, 5f, 5f);
        anim.setInterpolator(new LinearInterpolator());
        anim.setRepeatCount(Animation.INFINITE);
        anim.setDuration(700);
        image.startAnimation(anim);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!mSharedPrefManager.getLoginStatus()){
                    //go login
                    Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                    startActivity(intent);
                    finish();
                }

                else {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }}

        },4000);
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_splash;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }


    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }


}
