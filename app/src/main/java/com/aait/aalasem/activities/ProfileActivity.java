package com.aait.aalasem.activities;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.App.Constant;
import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.Utils.PermissionUtils;
import com.aait.aalasem.Utils.ProgressRequestBody;
import com.aait.aalasem.Views.ListDialog;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.listeners.OnItemClickListener;
import com.aait.aalasem.models.ActiveUserModel;
import com.aait.aalasem.models.ListModel;
import com.aait.aalasem.models.ProfileResponse;
import com.aait.aalasem.models.UserModel;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.aalasem.App.Constant.RequestPermission.REQUEST_IMAGES;

public class ProfileActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks, OnItemClickListener {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.ed_first_name)
    EditText first_name;
    @BindView(R.id.ed_last_name)
    EditText last_name;
    @BindView(R.id.ed_password)
    EditText password;
    @BindView(R.id.ed_phone)
    EditText phone;
    @BindView(R.id.ed_address)
    TextView address;
    @BindView(R.id.ed_work)
    EditText work;
    @BindView(R.id.check)
    CheckBox check;
    @BindView(R.id.ed_meddile_name)
    EditText ed_meddile_name;
    @BindView(R.id.ed_social_state)
    TextView ed_social_state;
    @BindView(R.id.ed_sons_num)
    EditText ed_sons_num;
    @BindView(R.id.ed_eduation)
    TextView ed_eduation;
    @BindView(R.id.civ_profile)
    CircleImageView civ_profile;
    String ImageBasePath = null;
    ArrayList<Uri> ImageList = new ArrayList<>();
    ListModel socialState ;
    ListModel _education ;
    ArrayList<ListModel> social = new ArrayList<>();
    ArrayList<ListModel> education = new ArrayList<>();
    ListDialog listDialog;
    int educationOrSocial = 0;
    String mAdresse="", mLng="", mLat = "";

    @OnClick(R.id.act_back)
    void onActBack(){
        onBackPressed();
    }
    ActiveUserModel userModel;

    @Override
    protected void initializeComponents() {
        act_title.setText("");
        social.add(new ListModel(1,"متزوج"));
        social.add(new ListModel(0,"اعزب"));
        education.add(new ListModel(1,"جامعى فاعلي"));
        education.add(new ListModel(2,"ثانوى فاقل"));
        getProfile();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_profile;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_cancel)
    void onCancel(){
        onBackPressed();
    }
    @OnClick(R.id.civ_profile)
    void onCivClick(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!(PermissionUtils.hasPermissions(mContext, Manifest.permission.CAMERA)&&PermissionUtils.hasPermissions(mContext,PermissionUtils.IMAGE_PERMISSIONS))) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        civ_profile.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }

    @OnClick(R.id.btn_update)
    void onUpdateClick(){
        if (validate()){
        if (check.isChecked()){
            if (ImageBasePath==null && password.getText().toString().equals("")){

                updateProfile(null,"yes");
            }else if (ImageBasePath==null &&!(password.getText().toString().equals(""))){
                if (password.getText().toString().length()<6){
                    CommonUtil.makeToast(mContext,getString(R.string.pass));
                }else {
                    updateProfile(password.getText().toString(), "yes");
                }
            }else if (ImageBasePath != null && password.getText().toString().equals("")){
                updateProfile(null,"yes",ImageBasePath);
            }else if (ImageBasePath!=null &&!(password.getText().toString().equals(""))){
                if (password.getText().toString().length()<6){
                    CommonUtil.makeToast(mContext,getString(R.string.pass));
                }else {
                    updateProfile(password.getText().toString(),"yes",ImageBasePath);
                }

            }
        }else {
            if (ImageBasePath==null && password.getText().toString().equals("")){
                updateProfile(null,"no");
            }else if (ImageBasePath==null &&!(password.getText().toString().equals(""))){
                if (password.getText().toString().length()<6){
                    CommonUtil.makeToast(mContext,getString(R.string.pass));
                }else {
                    updateProfile(password.getText().toString(), "no");
                }
            }else if (ImageBasePath != null && password.getText().toString().equals("")){
                updateProfile(null,"no",ImageBasePath);
            }else if (ImageBasePath!=null &&!(password.getText().toString().equals(""))){
                if (password.getText().toString().length()<6){
                    CommonUtil.makeToast(mContext,getString(R.string.pass));
                }else {
                    updateProfile(password.getText().toString(), "no", ImageBasePath);
                }
            }
        }}
    }

    private void getProfile(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProfile(Urls.Bearer+mSharedPrefManager.getUserData().getToken()).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){

                        act_title.setText(response.body().getData().getFirst_name()+" "+response.body().getData().getMiddle_name()+" "+response.body().getData().getLast_name());
                        first_name.setText(response.body().getData().getFirst_name());
                        last_name.setText(response.body().getData().getLast_name());
                        address.setText(response.body().getData().getLocation());
                        phone.setText(response.body().getData().getPhone());
                        work.setText(response.body().getData().getJop());
                        ed_meddile_name.setText(response.body().getData().getMiddle_name());
                        ed_sons_num.setText(response.body().getData().getSons_count());
                        if (response.body().getData().getEducational_qualification().equals("1")){
                            ed_eduation.setText("جامعى فاعلى");
                            _education = new ListModel(Integer.parseInt(response.body().getData().getEducational_qualification()),"جامعى فاعلى");
                        }else if (response.body().getData().getEducational_qualification().equals("2")){
                            ed_eduation.setText("ثانوى فاقل");
                            _education = new ListModel(Integer.parseInt(response.body().getData().getEducational_qualification()),"ثانوى فاقل");
                        }
                        if (response.body().getData().getSocial_status().equals("0")){
                            ed_social_state.setText("اعزب");
                            socialState = new ListModel(Integer.parseInt(response.body().getData().getSocial_status())," اعزب");

                        }else if (response.body().getData().getSocial_status().equals("1")){
                            ed_social_state.setText("متزوج");
                            socialState = new ListModel(Integer.parseInt(response.body().getData().getSocial_status())," متزوج");
                        }
                        Glide.with(mContext).load(response.body().getData().getAvatar()).asBitmap().placeholder(R.mipmap.treei).into(civ_profile);
                        if (response.body().getData().getIn_dictionary()==1){
                            check.setChecked(true);
                        }else {
                             check.setChecked(false);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void updateProfile(String password ,String checked){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).updateProfile(Urls.Bearer+mSharedPrefManager.getUserData().getToken(),first_name.getText().toString(),last_name.getText().toString(),phone.getText().toString(),password,checked,address.getText().toString(),work.getText().toString(),socialState.getId()+"",ed_sons_num.getText().toString(),_education.getId()+"",mLat,mLng,ed_meddile_name.getText().toString()).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        CommonUtil.makeToast(mContext,getString(R.string.profile_updated));
                        act_title.setText(response.body().getData().getFirst_name()+" "+response.body().getData().getMiddle_name()+" "+response.body().getData().getLast_name());
                        first_name.setText(response.body().getData().getFirst_name());
                        last_name.setText(response.body().getData().getLast_name());
                        address.setText(response.body().getData().getLocation());
                        phone.setText(response.body().getData().getPhone());
                        work.setText(response.body().getData().getJop());
                        ed_meddile_name.setText(response.body().getData().getMiddle_name());
                        ed_sons_num.setText(response.body().getData().getSons_count());
                        if (response.body().getData().getEducational_qualification().equals("1")){
                            ed_eduation.setText("جامعى فاعلى");
                            _education = new ListModel(Integer.parseInt(response.body().getData().getEducational_qualification()),"جامعى فاعلى");
                        }else if (response.body().getData().getEducational_qualification().equals("2")){
                            ed_eduation.setText("ثانوى فاقل");
                            _education = new ListModel(Integer.parseInt(response.body().getData().getEducational_qualification()),"ثانوى فاقل");
                        }
                        if (response.body().getData().getSocial_status().equals("0")){
                            ed_social_state.setText("اعزب");
                            socialState = new ListModel(Integer.parseInt(response.body().getData().getSocial_status())," اعزب");

                        }else if (response.body().getData().getSocial_status().equals("1")){
                            ed_social_state.setText("متزوج");
                            socialState = new ListModel(Integer.parseInt(response.body().getData().getSocial_status())," متزوج");
                        }
                        Glide.with(mContext).load(response.body().getData().getAvatar()).asBitmap().placeholder(R.mipmap.treei).into(civ_profile);
                        if (response.body().getData().getIn_dictionary()==1){
                            check.setChecked(true);
                        }else {
                            check.setChecked(false);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void updateProfile(String password ,String checked,String PathFromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, ProfileActivity.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).updateProfilewithImage(Urls.Bearer+mSharedPrefManager.getUserData().getToken(),first_name.getText().toString(),last_name.getText().toString(),phone.getText().toString(),password,checked,address.getText().toString(),work.getText().toString(),socialState.getId()+"",ed_sons_num.getText().toString(),_education.getId()+"",mLat,mLng,ed_meddile_name.getText().toString(),filePart).enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        CommonUtil.makeToast(mContext,getString(R.string.profile_updated));
                        act_title.setText(response.body().getData().getFirst_name()+" "+response.body().getData().getMiddle_name()+" "+response.body().getData().getLast_name());
                        first_name.setText(response.body().getData().getFirst_name());
                        last_name.setText(response.body().getData().getLast_name());
                        address.setText(response.body().getData().getLocation());
                        phone.setText(response.body().getData().getPhone());
                        work.setText(response.body().getData().getJop());
                        ed_meddile_name.setText(response.body().getData().getMiddle_name());
                        ed_sons_num.setText(response.body().getData().getSons_count());
                        if (response.body().getData().getEducational_qualification().equals("1")){
                            ed_eduation.setText("جامعى فاعلى");
                            _education = new ListModel(Integer.parseInt(response.body().getData().getEducational_qualification()),"جامعى فاعلى");
                        }else if (response.body().getData().getEducational_qualification().equals("2")){
                            ed_eduation.setText("ثانوى فاقل");
                            _education = new ListModel(Integer.parseInt(response.body().getData().getEducational_qualification()),"ثانوى فاقل");
                        }
                        if (response.body().getData().getSocial_status().equals("0")){
                            ed_social_state.setText("اعزب");
                            socialState = new ListModel(Integer.parseInt(response.body().getData().getSocial_status())," اعزب");

                        }else if (response.body().getData().getSocial_status().equals("1")){
                            ed_social_state.setText("متزوج");
                            socialState = new ListModel(Integer.parseInt(response.body().getData().getSocial_status())," متزوج");
                        }
                        Glide.with(mContext).load(response.body().getData().getAvatar()).asBitmap().placeholder(R.mipmap.treei).into(civ_profile);
                        if (response.body().getData().getIn_dictionary()==1){
                            check.setChecked(true);
                        }else {
                            check.setChecked(false);
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
    boolean validate(){
         if (first_name.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.firstName));
            return false;
        }else if (last_name.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.lastName));
            return false;
        }else if (phone.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.phoneNumber));
            return false;
        }
        else if(phone.getText().toString().length()<10){
             CommonUtil.makeToast(mContext,getString(R.string.phoneNumber));
             return false;
         }
        else if (address.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.Location));
            return false;
        }else if (work.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.Work));
            return false;
        }else {
            return true;
        }
    }
    @OnClick(R.id.ed_social_state)
    void onSocialClick(){
        educationOrSocial = 0;

        Log.e("social", new Gson().toJson(social));
        listDialog = new ListDialog(mContext,ProfileActivity.this,social,getString(R.string.social_state));
        listDialog.show();
    }
    @OnClick(R.id.ed_eduation)
    void onEducationClick(){
        educationOrSocial = 1;

        Log.e("education",new Gson().toJson(education));
        listDialog = new ListDialog(mContext,ProfileActivity.this,education,getString(R.string.education));
        listDialog.show();
    }
    @OnClick(R.id.ed_address)
    void onAddressClick(){
        MapDetectLocationActivity.startActivityForResult((AppCompatActivity)mContext);
    }

    @Override
    public void onItemClick(View view, int position) {
        listDialog.dismiss();
        if (educationOrSocial==0){
            socialState = social.get(position);
            ed_social_state.setText(socialState.getName());
            Log.e("id",socialState.getId()+"");
        }else if (educationOrSocial==1){
            _education = education.get(position);
            ed_eduation.setText(_education.getName());
            Log.e("idd",_education.getId()+"");
        }

    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constant.RequestCode.GET_LOCATION) {
                if (resultCode == RESULT_OK) {
                    mAdresse = data.getStringExtra(Constant.LocationConstant.LOCATION);
                    mLng = data.getStringExtra(Constant.LocationConstant.LNG);
                    mLat = data.getStringExtra(Constant.LocationConstant.LAT);
                    CommonUtil.PrintLogE("Lat : " + mLat + " Lng : " + mLng + " Address : " + mAdresse);
                    address.setText(mAdresse);
                }
            }
        }
    }
}
