package com.aait.aalasem.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;


import com.aait.aalasem.App.Constant;
import com.aait.aalasem.GPS.GPSTracker;
import com.aait.aalasem.GPS.GpsTrakerListener;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.Utils.DialogUtil;
import com.aait.aalasem.Utils.PermissionUtils;
import com.aait.aalasem.base.ParentActivity;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.OnClick;

public class DetectLocationActivity extends ParentActivity
        implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GpsTrakerListener {

    @BindView(R.id.map)
    MapView mapView;
    @OnClick(R.id.act_back)
            void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;


    GoogleMap googleMap;

    Marker myMarker;

    GPSTracker gps;

    public String mLang, mLat;

    private AlertDialog mAlertDialog;

    boolean startTracker = false;

    String mResult;



    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.detect_location));
        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_map_detect_location;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }


    @Override
    protected boolean isFullScreen() {
        return false;
    }



    @Override
    protected boolean isEnableBack() {
        return true;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }




    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        getLocationWithPermission();
    }

    @Override
    public void onMapClick(LatLng latLng) {
        Log.e("LatLng", latLng.toString());
        mLang = Double.toString(latLng.latitude);
        mLat = Double.toString(latLng.longitude);
        if (myMarker != null) {
            myMarker.remove();
            putMapMarker(latLng.latitude, latLng.longitude);
        } else {
            putMapMarker(latLng.latitude, latLng.longitude);
        }
    }

    @OnClick(R.id.btn_detect)
    void onDetectedSuccess() {
        Log.e("Location", "Lat:" + mLat + " Lng:" + mLang );
        if (mLang != null && mLat != null ) {
            Intent intentData = new Intent();
            intentData.putExtra(Constant.LocationConstant.LOCATION, getString(R.string.location_detected));
            intentData.putExtra(Constant.LocationConstant.LAT, mLat);
            intentData.putExtra(Constant.LocationConstant.LNG, mLang);
            setResult(RESULT_OK, intentData);
            finish();
        }

    }


    public void putMapMarker(Double lat, Double log) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        // getLocationInfo("" + lat, "" + log, "ar");
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.marker_));
        marker.title("موقعي");
        myMarker = googleMap.addMarker(marker);
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {
        Log.e("Direction", "Direction Success");
        // dismiss traker dialog
        if (startTracker) {
            if (lat != 0.0 && log != 0.0) {
                hideProgressDialog();
                Log.e("LATLNG", "Lat:" + mLat + "  Lng:" + Double.toString(log));
                putMapMarker(lat, log);
            }
        }
    }

    @Override
    public void onStartTracker() {
        startTracker = true;
        //showProgressDialog(getString(R.string.detecting_location));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.e("Code", "requestCode: " + requestCode);
        switch (requestCode) {
            case Constant.RequestCode.GET_LOCATION: {
                if (resultCode == RESULT_OK) {
                    Log.e("Code", "request GPS Enabled True");
                    getCurrentLocation();
                }
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int permsRequestCode, String[] permissions, int[] grantResults) {
        switch (permsRequestCode) {
            case 800: {
                if (grantResults.length > 0) {
                    boolean Locationpermission = (grantResults[0] == PackageManager.PERMISSION_GRANTED);
                    Log.e("Permission", "All permission are granted");
                    getCurrentLocation();
                    for (int i = 0; i < grantResults.length; i++) {
                        Log.e("Permission", grantResults[0] + "");
                    }
                } else {
                    Log.e("Permission", "permission arn't granted");
                }
                return;
            }
        }
    }


    public void getLocationWithPermission() {
        gps = new GPSTracker(this, this);
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(DetectLocationActivity.this, PermissionUtils.GPS_PERMISSION)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.GPS_PERMISSION,
                            Constant.RequestPermission.REQUEST_GPS_LOCATION);
                    Log.e("GPS", "1");
                }
            } else {
                getCurrentLocation();
                Log.e("GPS", "2");
            }
        } else {
            Log.e("GPS", "3");
            getCurrentLocation();
        }

    }

    void getCurrentLocation() {
        gps.getLocation();
        if (!gps.canGetLocation()) {
            mAlertDialog = DialogUtil.showAlertDialog(DetectLocationActivity.this,
                    getString(R.string.gps_detecting), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(final DialogInterface dialogInterface, final int i) {
                            mAlertDialog.dismiss();
                            Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            startActivityForResult(intent, Constant.RequestCode.GPS_ENABLING);
                        }
                    }, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });
        } else {
            if (gps.getLatitude() != 0.0 && gps.getLongitude() != 0.0) {
                putMapMarker(gps.getLatitude(), gps.getLongitude());
                mLat = String.valueOf(gps.getLatitude());
                mLang = String.valueOf(gps.getLongitude());
            }
        }
    }

    public static void startActivityForResult(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, DetectLocationActivity.class);
        appCompatActivity.startActivityForResult(intent, Constant.RequestCode.GET_LOCATION);
    }


    public static void startActivity(AppCompatActivity appCompatActivity) {
        Intent intent = new Intent(appCompatActivity, DetectLocationActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
        appCompatActivity.startActivity(intent);
    }

}

