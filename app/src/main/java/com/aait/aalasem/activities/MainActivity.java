package com.aait.aalasem.activities;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.widget.DrawerLayout;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.fragments.NavigationFragment;
import com.aait.aalasem.listeners.DrawerListner;
import com.aait.aalasem.models.NotificationCount;
import com.aait.aalasem.models.SocialResponse;

import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends ParentActivity implements DrawerListner {
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    NavigationFragment mNavigationFragment;
    @BindView(R.id.iv_menu)
    ImageView iv_menu;
    @BindView(R.id.count)
    TextView count;
    String face;
    String twitter;
    String youtube;
    @Override
    protected void initializeComponents() {
        mNavigationFragment = NavigationFragment.newInstance();
        mNavigationFragment.setDrawerListner(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.nav_view, mNavigationFragment).commit();
        getount();
        getSocial();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }



    @Override
    public void OpenCloseDrawer() {
        mNavigationFragment.setNavData();
        if (Locale.getDefault().equals("en")) {
            if (drawerLayout.isDrawerOpen(Gravity.START)) {
                drawerLayout.closeDrawer(Gravity.START);
            } else {
                drawerLayout.openDrawer(Gravity.START);
            }
        }else {
            if (drawerLayout.isDrawerOpen(Gravity.START)) {
                drawerLayout.closeDrawer(Gravity.START);
            } else {
                drawerLayout.openDrawer(Gravity.START);
            }
        }
    }
    @OnClick(R.id.iv_menu)
    void onMenuClick(){
        OpenCloseDrawer();
    }
    @OnClick(R.id.ic_notification)
    void onNotificationClick(){
        startActivity(new Intent(mContext,NotificationActivity.class));

    }
    @OnClick(R.id.dalel)
    void onDalelClick(){
        startActivity(new Intent(mContext,DalelAlkabelaActivity.class));

    }
    @OnClick(R.id.tree)
    void onTreeClick(){
        startActivity(new Intent(mContext,KabelaTreeActivity.class));

    }
    @OnClick(R.id.box)
    void onBoxClick(){
        startActivity(new Intent(mContext,KabelaBoxActivity.class));

    }
    @OnClick(R.id.historical_heritage)
    void onHistoryClick(){
        startActivity(new Intent(mContext,HistoricalHeritageActivity.class));
           }
    @OnClick(R.id.production)
    void onProductionClick(){
        startActivity(new Intent(mContext,ProductionActivity.class));

    }
    @OnClick(R.id.news)
    void onNewsClick(){
        startActivity(new Intent(mContext,NewsAndMediaActivity.class));

    }
    @OnClick(R.id.statistics)
    void onStatisticsClick(){
        startActivity(new Intent(mContext,StatisticsActivity.class));

    }
    @OnClick(R.id.monasbat)
    void onMonasabatClick(){
        startActivity(new Intent(mContext,MonasbatAlkabelaActivity.class));

    }
    @OnClick(R.id.employment)
    void onEmploymentClick(){
        startActivity(new Intent(mContext,EmploymentActivity.class));

    }
    @OnClick(R.id.ills)
    void onIllsClick(){
        startActivity(new Intent(mContext,IllsActivity.class));

    }
    @OnClick(R.id.dead)
    void onDeadClick(){
        startActivity(new Intent(mContext,DeadActivity.class));

    }
    @OnClick(R.id.face)
    void onFace(){
        if (!face.equals("")){
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(face)));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.no_data));
        }

    }
    @OnClick(R.id.twitter)
    void onTwitter(){
        if (!twitter.equals("")){
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(twitter)));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.no_data));
        }

    }
    @OnClick(R.id.youtube)
    void onYouTube(){
        if (!youtube.equals("")){
            startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse(youtube)));
        }else {
            CommonUtil.makeToast(mContext,getString(R.string.no_data));
        }
    }
    private void getount(){
        RetroWeb.getClient().create(ServiceApi.class).getCount(Urls.Bearer+mSharedPrefManager.getUserData().getToken()).enqueue(new Callback<NotificationCount>() {
            @Override
            public void onResponse(Call<NotificationCount> call, Response<NotificationCount> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        if (response.body().getData()>0){
                            count.setVisibility(View.VISIBLE);
                            count.setText(response.body().getData()+"");
                        }

                    }
                    else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<NotificationCount> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();


            }
        });
    }
    private void getSocial(){
        RetroWeb.getClient().create(ServiceApi.class).getSocial().enqueue(new Callback<SocialResponse>() {
            @Override
            public void onResponse(Call<SocialResponse> call, Response<SocialResponse> response) {
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        face=response.body().getData().getFacebook();
                        twitter = response.body().getData().getTwitter();
                        youtube = response.body().getData().getYoutube();

                    }else {

                    }
                }
            }

            @Override
            public void onFailure(Call<SocialResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }

}
