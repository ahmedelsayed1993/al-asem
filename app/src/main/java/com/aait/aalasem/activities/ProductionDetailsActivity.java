package com.aait.aalasem.activities;

import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.ProductionDetailsResponse;
import com.bumptech.glide.Glide;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductionDetailsActivity extends ParentActivity {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.cir_image)
    CircleImageView cir_image;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.body)
    TextView body;
    String id;


    @Override
    protected void initializeComponents() {
        id = getIntent().getStringExtra("id");
        getProduction();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_production_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getProduction(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProd(Integer.parseInt(id)).enqueue(new Callback<ProductionDetailsResponse>() {
            @Override
            public void onResponse(Call<ProductionDetailsResponse> call, Response<ProductionDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        act_title.setText(response.body().getData().getTitle());
                        title.setText(response.body().getData().getTitle());
                        body.setText(response.body().getData().getBody());
                        Glide.with(mContext).load(response.body().getData().getImage()).asBitmap().placeholder(R.mipmap.mlogo).into(cir_image);
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<ProductionDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
