package com.aait.aalasem.activities;

import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.GPS.GPSTracker;
import com.aait.aalasem.GPS.GpsTrakerListener;
import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.UserDataModel;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;

public class UserLocationActivity extends ParentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener,
        GpsTrakerListener {

    @BindView(R.id.map)
    MapView mapView;
    GoogleMap mgoogleMap;

    Marker myMarker;

    GPSTracker gps;
    boolean startTracker = false;
    String id;
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBackClick(){
        onBackPressed();
    }
    UserDataModel userDataModel;
    String lat;
    String lng;
    String address;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.kabela_dalel));
        lat = getIntent().getStringExtra("lat");
        lng = getIntent().getStringExtra("lng");
        address = getIntent().getStringExtra("address");

        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
       draw();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_monasba_location;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onTrackerSuccess(Double lat, Double log) {

    }

    @Override
    public void onStartTracker() {
        startTracker = true;
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.mgoogleMap = googleMap;
        googleMap.setOnMapClickListener(this);
    }
    public void putMapMarker(Double lat, Double log,String title) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        // getLocationInfo("" + lat, "" + log, "ar");
        LatLng latLng = new LatLng(lat, log);
        Log.e("lat",latLng+"");
        mgoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.marker_));
        marker.title(title);
        myMarker = mgoogleMap.addMarker(marker);
    }
    private void draw(){

        if (lat.equals("")&&lng.equals("")){
              putMapMarker(0.0,0.0,address);
        }else {
            putMapMarker(Double.parseDouble(lat), Double.parseDouble(lng), address);
        }
    }
}
