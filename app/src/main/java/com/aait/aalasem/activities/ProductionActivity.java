package com.aait.aalasem.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.adapters.JobsAdapter;
import com.aait.aalasem.adapters.ProductionAdapter;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.listeners.OnItemClickListener;
import com.aait.aalasem.models.GetProductionResponse;
import com.aait.aalasem.models.JopsModel;
import com.aait.aalasem.models.ProductionModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProductionActivity extends ParentActivity {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        startActivity(new Intent(mContext,MainActivity.class));
    }
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    GridLayoutManager gridLayoutManager;
    ArrayList<ProductionModel> jopsModels = new ArrayList<>();
    ProductionAdapter jobsAdapter;

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.production));
        gridLayoutManager = new GridLayoutManager(mContext,3);
        jobsAdapter = new ProductionAdapter(mContext,jopsModels,R.layout.recycler_jobs);
        rvRecycle.setLayoutManager(gridLayoutManager);
        rvRecycle.setAdapter(jobsAdapter);
        getProduction();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_production;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.add_new)
    void onAddNewClick(){
        startActivity(new Intent(mContext,AddProductionActivity.class));
    }
    private void getProduction(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getProduction("productions").enqueue(new Callback<GetProductionResponse>() {
            @Override
            public void onResponse(Call<GetProductionResponse> call, final Response<GetProductionResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        if (response.body().getData().getProductions().size()==0){
                            CommonUtil.makeToast(mContext,getString(R.string.no_data));
                        }else {
                            jobsAdapter.updateAll(response.body().getData().getProductions());
                            jobsAdapter.setOnItemClickListener(new OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent intent = new Intent(mContext,ProductionDetailsActivity.class);
                                    intent.putExtra("id",response.body().getData().getProductions().get(position).getId()+"");
                                    startActivity(intent);
                                }
                            });
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<GetProductionResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
