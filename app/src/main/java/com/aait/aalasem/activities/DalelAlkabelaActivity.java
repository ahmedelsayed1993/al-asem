package com.aait.aalasem.activities;

import android.content.Intent;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.adapters.DictionaryAdapter;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.listeners.OnItemClickListener;
import com.aait.aalasem.models.DictionaryResponse;
import com.aait.aalasem.models.UserDataModel;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DalelAlkabelaActivity extends ParentActivity  implements OnItemClickListener {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        startActivity(new Intent(mContext,MainActivity.class));
    }
    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;
    @BindView(R.id.lay_progress)
    RelativeLayout layProgress;
    @BindView(R.id.lay_no_internet)
    RelativeLayout layNoInternet;
    @BindView(R.id.lay_no_item)
    RelativeLayout layNoItem;
    @BindView(R.id.tv_no_content)
    TextView tvNoContent;
    @BindView(R.id.swipe_refresh)
    SwipeRefreshLayout swipeRefresh;
    @BindView(R.id.ed_search)
    EditText ed_search;
    LinearLayoutManager linearLayoutManager;
    DictionaryAdapter dictionaryAdapter;
    ArrayList<UserDataModel> userDataModels = new ArrayList<>();

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.kabela_dalel));
        linearLayoutManager = new LinearLayoutManager(mContext,LinearLayoutManager.VERTICAL,false);
        dictionaryAdapter = new DictionaryAdapter(mContext,userDataModels,R.layout.recycler_dectionary);
        dictionaryAdapter.setOnItemClickListener(this);
        rvRecycle.setLayoutManager(linearLayoutManager);
        rvRecycle.setAdapter(dictionaryAdapter);
        swipeRefresh.setColorSchemeResources(R.color.orange, R.color.green, R.color.blue);
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getDictionary();
            }
        });
        getDictionary();


    }
    @OnClick(R.id.search)
    void onSearchClick(){
        search();
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_dalel_alkabela;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void search(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).search(ed_search.getText().toString()).enqueue(new Callback<DictionaryResponse>() {
            @Override
            public void onResponse(Call<DictionaryResponse> call, Response<DictionaryResponse> response) {
                hideProgressDialog();
                ed_search.setText("");
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        if (response.body().getData().size()==0) {

                        CommonUtil.makeToast(mContext,getString(R.string.no_data));

                        }else {
                            dictionaryAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<DictionaryResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void getDictionary(){
        layProgress.setVisibility(View.VISIBLE);
        layNoInternet.setVisibility(View.GONE);
        layNoItem.setVisibility(View.GONE);
        RetroWeb.getClient().create(ServiceApi.class).getDictionary().enqueue(new Callback<DictionaryResponse>() {
            @Override
            public void onResponse(Call<DictionaryResponse> call, Response<DictionaryResponse> response) {
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        if (response.body().getData().size()==0){
                            layNoItem.setVisibility(View.VISIBLE);
                            layNoInternet.setVisibility(View.GONE);
                            tvNoContent.setText(R.string.no_data);
                        }else {
                            dictionaryAdapter.updateAll(response.body().getData());
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<DictionaryResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                layNoInternet.setVisibility(View.VISIBLE);
                layNoItem.setVisibility(View.GONE);
                layProgress.setVisibility(View.GONE);
                swipeRefresh.setRefreshing(false);

            }
        });
    }

    @Override
    public void onItemClick(View view, int position) {
        if (view.getId()==R.id.details_lay) {
            Intent intent = new Intent(mContext, UserActivity.class);
            intent.putExtra("user", userDataModels.get(position));
            startActivity(intent);
        }else if (view.getId()==R.id.location_lay){
            Intent intent = new Intent(mContext, MonasbaLocationActivity.class);
            intent.putExtra("lat",userDataModels.get(position).getLat());
            intent.putExtra("lng",userDataModels.get(position).getLng());
            intent.putExtra("address",userDataModels.get(position).getLocation());
            intent.putExtra("type","user");

            startActivity(intent);
        }


    }
}
