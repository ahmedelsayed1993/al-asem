package com.aait.aalasem.activities;

import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.UserDataModel;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserActivity extends ParentActivity {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        onBackPressed();
    }
    @BindView(R.id.name)
    TextView first_name;


    @BindView(R.id.phone)
    TextView phone;
    @BindView(R.id.location)
    TextView address;
    @BindView(R.id.work)
    TextView work;


    @BindView(R.id.social_state)
    TextView ed_social_state;
    @BindView(R.id.son_num)
    TextView ed_sons_num;
    @BindView(R.id.education)
    TextView ed_eduation;
    UserDataModel userDataModel;


    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.kabela_dalel));
        userDataModel = (UserDataModel) getIntent().getSerializableExtra("user");
        Log.e("user",new Gson().toJson(userDataModel));
        first_name.setText(userDataModel.getFirst_name()+" "+userDataModel.getMiddle_name()+" "+userDataModel.getLast_name());
        phone.setText(userDataModel.getPhone());
        work.setText(userDataModel.getJop());
        if (userDataModel.getSocial_status().equals("")){
            ed_social_state.setText("");
        }else if (userDataModel.getSocial_status().equals("1")){
            ed_social_state.setText("متزوج");
        }else if (userDataModel.getSocial_status().equals("0")){
            ed_social_state.setText("اعزب");
        }
        address.setText(userDataModel.getLocation());
        ed_sons_num.setText(userDataModel.getSons_count());
        if (userDataModel.getEducational_qualification().equals("")){
            ed_eduation.setText("");
        }else if (userDataModel.getSocial_status().equals("1")){
            ed_eduation.setText("جامعى فاعلى");
        }else if (userDataModel.getSocial_status().equals("2")){
            ed_eduation.setText("ثانوى فاقل");
        }

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_user;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
}
