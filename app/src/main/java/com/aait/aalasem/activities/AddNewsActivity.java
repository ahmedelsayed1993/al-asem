package com.aait.aalasem.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.Utils.PermissionUtils;
import com.aait.aalasem.Utils.ProgressRequestBody;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.BaseResponse;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.aalasem.App.Constant.RequestPermission.REQUEST_IMAGES;

public class AddNewsActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        onBackPressed();
    }
    @BindView(R.id.ed_address)
    EditText ed_address;
    @BindView(R.id.ed_details)
    EditText ed_details;
    @BindView(R.id.lay_image)
    ImageView lay_image;
    @BindView(R.id.news_image)
    ImageView news_image;
    @BindView(R.id.add_image)
    ImageView add_image;
    @BindView(R.id.btn_send)
    Button btn_send;
    String ImageBasePath = null;
    ArrayList<Uri> ImageList = new ArrayList<>();
    ArrayList<String> images = new ArrayList<>();

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.media));


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_add_new_news;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.add_image)
    void onAddClick(){
        getPickImageWithPermission();
    }
    @OnClick(R.id.btn_send)
    void onSendClick(){
        if (ed_address.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.news_address));
        }else if (ed_details.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.news_details));
        }else {
            if (ImageBasePath!=null) {
                AddNews(images);
            }else if (ImageBasePath==null){
                AddNews();
            }
        }
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        for (int i =0;i<ImageList.size();i++){
                            images.add(ImageList.get(i).getPath());
                        }
                        ImageBasePath = ImageList.get(0).getPath();
                        lay_image.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(15)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }
    private void AddNews(ArrayList<String> paths){
        List<MultipartBody.Part> img = new ArrayList<>();
        showProgressDialog(getString(R.string.please_wait));
        for (String photo :paths) {
            MultipartBody.Part filePart = null;
            File ImageFile = new File(photo);
            ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, AddNewsActivity.this);
            filePart = MultipartBody.Part.createFormData("images[]", ImageFile.getName(), fileBody);
            img.add(filePart);
        }
        Log.e("token",Urls.Bearer+mSharedPrefManager.getUserData().getToken());
            RetroWeb.getClient().create(ServiceApi.class).addNew(Urls.Bearer+mSharedPrefManager.getUserData().getToken(),ed_address.getText().toString(),ed_details.getText().toString(),img).enqueue(new Callback<BaseResponse>() {
                @Override
                public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                    hideProgressDialog();
                    if (response.isSuccessful()){
                        if (response.body().getStatus().equals("1")){
                            CommonUtil.makeToast(mContext,getString(R.string.waiting_admin));
                            startActivity(new Intent(mContext,MainActivity.class));
                        }else {
                            CommonUtil.makeToast(mContext,response.body().getMsg());
                        }
                    }
                }

                @Override
                public void onFailure(Call<BaseResponse> call, Throwable t) {
                    CommonUtil.handleException(mContext,t);
                    t.printStackTrace();
                    hideProgressDialog();

                }
            });

    }
    private void AddNews(){

        showProgressDialog(getString(R.string.please_wait));

        Log.e("token",Urls.Bearer+mSharedPrefManager.getUserData().getToken());
        RetroWeb.getClient().create(ServiceApi.class).AddNew(Urls.Bearer+mSharedPrefManager.getUserData().getToken(),ed_address.getText().toString(),ed_details.getText().toString()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        CommonUtil.makeToast(mContext,getString(R.string.waiting_admin));
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });

    }

    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }
}
