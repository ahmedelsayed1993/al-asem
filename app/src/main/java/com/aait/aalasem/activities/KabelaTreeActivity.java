package com.aait.aalasem.activities;

import android.content.Intent;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class KabelaTreeActivity extends ParentActivity {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        startActivity(new Intent(mContext,MainActivity.class));
    }
    @BindView(R.id._webview)
    WebView mWebView;

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.kabela_tree));
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        mWebView.loadUrl("http://alathem.aait-sa.com/tree");


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_kabela_tree;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
}
