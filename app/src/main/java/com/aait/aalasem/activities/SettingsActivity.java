package com.aait.aalasem.activities;

import android.support.v7.widget.SwitchCompat;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentActivity;

import butterknife.BindView;
import butterknife.OnClick;

public class SettingsActivity extends ParentActivity {
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onActBack(){
        onBackPressed();
    }
    @BindView(R.id.switch_notification)
    SwitchCompat switchNotification;

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.settings));
        setLanguageData();
        switchNotification.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(final CompoundButton compoundButton, final boolean b) {
                mSharedPrefManager.setNotificationStatus(b);

            }
        });


    }
    @Override
    protected int getLayoutResource() {
        return R.layout.activity_settings;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    void setLanguageData() {

        if (mSharedPrefManager.getNotificationStatus()) {
            switchNotification.setChecked(true);
        } else {
            switchNotification.setChecked(false);
        }
    }
}
