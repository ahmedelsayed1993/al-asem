package com.aait.aalasem.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.Utils.PermissionUtils;
import com.aait.aalasem.Utils.ProgressRequestBody;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.BanckResponse;
import com.aait.aalasem.models.BaseResponse;

import java.io.File;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import gun0912.tedbottompicker.TedBottomPicker;
import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.aait.aalasem.App.Constant.RequestPermission.REQUEST_IMAGES;

public class TransferActivity extends ParentActivity implements ProgressRequestBody.UploadCallbacks {
    @BindView(R.id.bank_account)
    TextView bank_account;
    @BindView(R.id.account_number)
    TextView account_number;
    @BindView(R.id.account_owner)
    EditText account_owner;
    @BindView(R.id.num_person)
    EditText num_person;
    @BindView(R.id.account_num)
    EditText account_num;
    @BindView(R.id.amount)
    EditText amount;
    @BindView(R.id.person_cost)
    TextView person_cost;
    @BindView(R.id.lay_image)
    ImageView lay_image;
    @BindView(R.id.add_image)
    ImageView add_image;
    @BindView(R.id.btn_send)
    Button btn_send;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    String ImageBasePath = null;
    ArrayList<Uri> ImageList = new ArrayList<>();
    @Override
    public void onProgressUpdate(int percentage) {

    }

    @Override
    public void onError() {

    }

    @Override
    public void onFinish() {

    }

    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.kabela_box));
        getBank();


    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_transfer;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    @OnClick(R.id.btn_send)
    void onSend(){
        if (account_owner.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.account_owner));
        }else if(num_person.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.person_num));
        }else if (account_num.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.account_number));
        }else if (amount.getText().toString().equals("")){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.amount));
        }else if (ImageBasePath==null){
            CommonUtil.makeToast(mContext,getString(R.string.please_enter)+" "+getString(R.string.bill_image));
        }else {
            Pay(ImageBasePath);
        }
    }

    @OnClick(R.id.add_image)
    void onAddClick(){
        getPickImageWithPermission();
    }
    public void getPickImageWithPermission() {
        if (PermissionUtils.canMakeSmores(Build.VERSION_CODES.LOLLIPOP_MR1)) {
            if (!PermissionUtils.hasPermissions(mContext, PermissionUtils.IMAGE_PERMISSIONS)) {
                CommonUtil.PrintLogE("Permission not granted");
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(PermissionUtils.IMAGE_PERMISSIONS,
                            REQUEST_IMAGES);
                }
            } else {
                pickMultiImages();
                CommonUtil.PrintLogE("Permission is granted before");
            }
        } else {
            CommonUtil.PrintLogE("SDK minimum than 23");
            pickMultiImages();
        }
    }

    void pickMultiImages() {
        TedBottomPicker bottomSheetDialogFragment = new TedBottomPicker.Builder(mContext)
                .setOnMultiImageSelectedListener(new TedBottomPicker.OnMultiImageSelectedListener() {
                    @Override
                    public void onImagesSelected(ArrayList<Uri> uriList) {
                        ImageList = uriList;
                        ImageBasePath = ImageList.get(0).getPath();
                        lay_image.setImageURI(Uri.parse(ImageBasePath));

                    }
                })
                .setTitle(R.string.avatar)
                .setSelectMaxCount(1)
                .setSelectMinCount(1)
                .setPeekHeight(2600)
                .showTitle(false)
                .setCompleteButtonText(R.string.choose)
                .setEmptySelectionText(R.string.no_item_selected_yet)
                .create();
        bottomSheetDialogFragment.show(getSupportFragmentManager());
    }
    private void getBank(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getBank(Urls.Bearer+mSharedPrefManager.getUserData().getToken()).enqueue(new Callback<BanckResponse>() {
            @Override
            public void onResponse(Call<BanckResponse> call, Response<BanckResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        bank_account.setText(getString(R.string.bank_account_number)+response.body().getData().getBank_name());
                        account_number.setText(response.body().getData().getBank_number());
                        person_cost.setText(getString(R.string.person_cost)+response.body().getData().getPerson_dontate_cost()+getString(R.string.SAR));

                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BanckResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void Pay(String PathFromImage){
        showProgressDialog(getString(R.string.please_wait));
        MultipartBody.Part filePart = null;
        File ImageFile = new File(PathFromImage);
        ProgressRequestBody fileBody = new ProgressRequestBody(ImageFile, TransferActivity.this);
        filePart = MultipartBody.Part.createFormData("image", ImageFile.getName(), fileBody);
        RetroWeb.getClient().create(ServiceApi.class).pay(Urls.Bearer+mSharedPrefManager.getUserData().getToken(),account_owner.getText().toString(),Integer.parseInt(num_person.getText().toString()),account_num.getText().toString(),amount.getText().toString(),filePart).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                        startActivity(new Intent(mContext,MainActivity.class));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
