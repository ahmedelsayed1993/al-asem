package com.aait.aalasem.activities;

import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.R;
import com.aait.aalasem.base.ParentActivity;
import com.bumptech.glide.Glide;


import butterknife.BindView;
import butterknife.OnClick;

public class ImagesActivity extends ParentActivity {
    @BindView(R.id.image)
    ImageView image;
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    String id;

    @Override
    protected void initializeComponents() {
        act_title.setText("");
        id = getIntent().getStringExtra("id");

        Glide.with(mContext).load(id).asBitmap().placeholder(R.mipmap.mlogo).into(image);

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_images;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

}
