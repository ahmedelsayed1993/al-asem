package com.aait.aalasem.activities;

import android.content.Intent;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.adapters.ImageAdapter;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.listeners.OnItemClickListener;
import com.aait.aalasem.models.NewDetailsResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewsDetailsActivity extends ParentActivity {
    @OnClick(R.id.act_back)
    void onBack(){
        onBackPressed();
    }
    @BindView(R.id.act_title)
    TextView act_title;
    @BindView(R.id.new_title)
            TextView new_title;
    @BindView(R.id.new_date)
            TextView new_date;
    @BindView(R.id.new_details)
            TextView new_details;
    @BindView(R.id.recycler_image)
    RecyclerView recycler_image;
    String id;
    ImageAdapter imageAdapter;
    ArrayList<String> images = new ArrayList<>();
    GridLayoutManager gridLayoutManager;
    @Override
    protected void initializeComponents() {
        act_title.setText(getString(R.string.media));
        id = getIntent().getStringExtra("id");
        gridLayoutManager =new GridLayoutManager(mContext,3);
        imageAdapter = new ImageAdapter(mContext,images,R.layout.recycler_images);
        recycler_image.setLayoutManager(gridLayoutManager);
        recycler_image.setAdapter(imageAdapter);
        getNews();

    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_news_details;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }
    private void getNews(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getNew(Integer.parseInt(id)).enqueue(new Callback<NewDetailsResponse>() {
            @Override
            public void onResponse(Call<NewDetailsResponse> call, final Response<NewDetailsResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        new_title.setText(response.body().getData().getTitle());
                        new_date.setText(response.body().getData().getUser()+"   "+response.body().getData().getCreated_at());
                        new_details.setText(response.body().getData().getBody());
                        if (response.body().getData().getImages().size()==0){

                        }else {
                            imageAdapter.updateAll(response.body().getData().getImages());
                            imageAdapter.setOnItemClickListener(new OnItemClickListener() {
                                @Override
                                public void onItemClick(View view, int position) {
                                    Intent intent = new Intent(mContext,ImagesActivity.class);
                                    intent.putExtra("id",response.body().getData().getImages().get(position));
                                    startActivity(intent);
                                }
                            });
                        }
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<NewDetailsResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
}
