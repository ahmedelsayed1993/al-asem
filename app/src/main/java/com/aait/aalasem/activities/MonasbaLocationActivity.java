package com.aait.aalasem.activities;

import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.aait.aalasem.GPS.GPSTracker;
import com.aait.aalasem.GPS.GpsTrakerListener;
import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.base.ParentActivity;
import com.aait.aalasem.models.MonasbaResponse;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MonasbaLocationActivity extends ParentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener,
        GpsTrakerListener {
    @BindView(R.id.map)
    MapView mapView;
    GoogleMap googleMap;

    Marker myMarker;

    GPSTracker gps;
    boolean startTracker = false;
    String id;
    @BindView(R.id.act_back)
    ImageView act_back;
    @BindView(R.id.act_title)
    TextView act_title;
    @OnClick(R.id.act_back)
    void onBackClick(){
        onBackPressed();
    }
    String lat;
    String lng;
    String maddress;
    String type;

    @Override
    public void onTrackerSuccess(Double lat, Double log) {

    }

    @Override
    public void onStartTracker() {
        startTracker = true;
    }

    @Override
    protected void initializeComponents() {
        type = getIntent().getStringExtra("type");
        Log.e("type",type);

            id = getIntent().getStringExtra("id");



        mapView.onCreate(mSavedInstanceState);
        mapView.onResume();
        mapView.getMapAsync(this);

        try {
            MapsInitializer.initialize(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (type.equals("monasba")) {
            getMonasba();
        }
    }

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_monasba_location;
    }

    @Override
    protected boolean isEnableToolbar() {
        return false;
    }

    @Override
    protected boolean isFullScreen() {
        return false;
    }

    @Override
    protected boolean isEnableBack() {
        return false;
    }

    @Override
    protected boolean hideInputType() {
        return false;
    }

    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        googleMap.setOnMapClickListener(this);
        if (type.equals("user")){
            act_title.setText(getString(R.string.kabela_dalel));
            draw();
        }

    }
    public void putMapMarker(Double lat, Double log,String title) {
        Log.e("LatLng:", "Lat: " + lat + " Lng: " + log);
        // getLocationInfo("" + lat, "" + log, "ar");
        LatLng latLng = new LatLng(lat, log);
        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, 15));
        MarkerOptions marker = new MarkerOptions().position(
                new LatLng(lat, log));
        marker.icon(BitmapDescriptorFactory
                .fromResource(R.mipmap.marker_));
        marker.title(title);
        myMarker = googleMap.addMarker(marker);
    }
    private void getMonasba(){
        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).getMonasba(Integer.parseInt(id)).enqueue(new Callback<MonasbaResponse>() {
            @Override
            public void onResponse(Call<MonasbaResponse> call, Response<MonasbaResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        putMapMarker(Double.parseDouble(response.body().getData().getLat()),Double.parseDouble(response.body().getData().getLng()),response.body().getData().getTitle());
                        act_title.setText(response.body().getData().getTitle());
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }
                }
            }

            @Override
            public void onFailure(Call<MonasbaResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();

            }
        });
    }
    private void draw(){
        lat = getIntent().getStringExtra("lat");
        lng = getIntent().getStringExtra("lng");
        maddress = getIntent().getStringExtra("address");
        if (lat.equals("")&&lng.equals("")){
            putMapMarker(0.0,0.0,maddress);
        }else {
            putMapMarker(Double.parseDouble(lat), Double.parseDouble(lng), maddress);
        }
    }
}
