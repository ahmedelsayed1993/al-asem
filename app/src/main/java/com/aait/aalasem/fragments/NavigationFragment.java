package com.aait.aalasem.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.aait.aalasem.Network.RetroWeb;
import com.aait.aalasem.Network.ServiceApi;
import com.aait.aalasem.Network.Urls;
import com.aait.aalasem.R;
import com.aait.aalasem.Utils.CommonUtil;
import com.aait.aalasem.Utils.DialogUtil;
import com.aait.aalasem.activities.AboutAppActivity;
import com.aait.aalasem.activities.ContactUsActivity;
import com.aait.aalasem.activities.LoginActivity;
import com.aait.aalasem.activities.MainActivity;
import com.aait.aalasem.activities.NotificationActivity;
import com.aait.aalasem.activities.ProfileActivity;
import com.aait.aalasem.activities.SettingsActivity;
import com.aait.aalasem.activities.SplashActivity;
import com.aait.aalasem.activities.TermsAndConditonsAvctivity;
import com.aait.aalasem.adapters.NavigationDrawerAdapter;
import com.aait.aalasem.base.BaseFragment;
import com.aait.aalasem.listeners.DrawerListner;
import com.aait.aalasem.listeners.OnItemClickListener;
import com.aait.aalasem.models.BaseResponse;
import com.aait.aalasem.models.NavigationModel;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NavigationFragment extends BaseFragment implements OnItemClickListener {

    @BindView(R.id.rv_recycle)
    RecyclerView rvRecycle;

    @BindView(R.id.lay_profile)
    LinearLayout lay_profile;



    @BindView(R.id.civ_user_image)
    CircleImageView civ_user_image;


    ArrayList<NavigationModel> mNavigationModels;

    NavigationDrawerAdapter drawerAdapter;

    DrawerListner drawerListner;

    public static NavigationFragment newInstance() {
        Bundle args = new Bundle();
        NavigationFragment fragment = new NavigationFragment();
        fragment.setArguments(args);
        return fragment;
    }
    @Override
    protected int getLayoutResource() {
        return R.layout.fragment_navigation_menu;
    }

    @Override
    protected void initializeComponents(View view) {
        setNavData();
        setMenuData();
    }

    @OnClick(R.id.civ_user_image)
    void onProfileClick(){
        startActivity(new Intent(mContext, ProfileActivity.class));
    }
    @Override
    protected boolean isRecycle() {
        return false;
    }

    @Override
    public void onItemClick(View view, int position) {

            switch (position) {
                case 0:
                    startActivity(new Intent(mContext, MainActivity.class));
                    break;
                case 1:
                    startActivity(new Intent(mContext, ContactUsActivity.class));
                    break;
                case 2:
                    startActivity(new Intent(mContext, NotificationActivity.class));
                    break;
                case 3:
                   startActivity(new Intent(mContext, TermsAndConditonsAvctivity.class));
                   break;
                case 4:
                    startActivity(new Intent(mContext, AboutAppActivity.class));
                    break;
                case 5:
                    CommonUtil.ShareApp(mContext);
                    break;
                case 6:
                    startActivity(new Intent(mContext, SettingsActivity.class));
                    break;
                case 11:

                    logout();
                    break;

            }

    }

    public void setDrawerListner(DrawerListner drawerListner) {
        this.drawerListner = drawerListner;
    }
    public void setMenuData() {
        mNavigationModels = new ArrayList<>();


        mNavigationModels.add(new NavigationModel(getString(R.string.main),R.mipmap.home));
        mNavigationModels.add(new NavigationModel(getString(R.string.contact_us),R.mipmap.call));
        mNavigationModels.add(new NavigationModel(getString(R.string.notification),R.mipmap.notf2));
        mNavigationModels.add(new NavigationModel(getString(R.string.terms_conditions),R.mipmap.conditions));
        mNavigationModels.add(new NavigationModel(getString(R.string.about_app),R.mipmap.about));
        mNavigationModels.add(new NavigationModel(getString(R.string.share),R.mipmap.share));
        mNavigationModels.add(new NavigationModel(getString(R.string.settings),R.mipmap.settings));
        //for space in drawable  4 spaces
        mNavigationModels.add(new NavigationModel("",0));
        mNavigationModels.add(new NavigationModel("",0));
        mNavigationModels.add(new NavigationModel("",0));
        mNavigationModels.add(new NavigationModel("",0));
        mNavigationModels.add(new NavigationModel(getString(R.string.logout),R.mipmap.logout));











        rvRecycle.setLayoutManager(new LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false));
        drawerAdapter = new NavigationDrawerAdapter(mContext, mNavigationModels,
                R.layout.recycler_navigation);
        drawerAdapter.setOnItemClickListener(this);
        rvRecycle.setAdapter(drawerAdapter);
    }
    public void setNavData() {
        if (mSharedPrefManager.getLoginStatus()) {

            Glide.with(mContext).load(mSharedPrefManager.getUserData().getUser().getAvatar()).asBitmap()
                    .placeholder(R.mipmap.mlogo).error(R.mipmap.mlogo).into(civ_user_image);
        } else {

            civ_user_image.setVisibility(View.GONE);
        }


    }
    private void logout(){

        showProgressDialog(getString(R.string.please_wait));
        RetroWeb.getClient().create(ServiceApi.class).logout(Urls.Bearer +mSharedPrefManager.getUserData().getToken()).enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                hideProgressDialog();
                if (response.isSuccessful()){
                    if (response.body().getStatus().equals("1")){
                        mSharedPrefManager.Logout();
                        startActivity(new Intent(mContext,SplashActivity.class));

                    }
                    else if (response.body().getStatus().equals("0") &&response.body().getMsg().equals("Token is Invalid")){
                        mSharedPrefManager.Logout();
                        startActivity(new Intent(mContext, SplashActivity.class));
                        CommonUtil.makeToast(mContext,getString(R.string.account_deleted));
                    }else {
                        CommonUtil.makeToast(mContext,response.body().getMsg());
                    }

                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                CommonUtil.handleException(mContext,t);
                t.printStackTrace();
                hideProgressDialog();
                mSharedPrefManager.Logout();
                startActivity(new Intent(mContext,SplashActivity.class));

            }
        });
    }
}

