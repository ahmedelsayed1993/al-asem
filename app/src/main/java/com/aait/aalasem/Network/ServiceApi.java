package com.aait.aalasem.Network;

import android.support.v7.widget.CardView;

import com.aait.aalasem.models.BanckResponse;
import com.aait.aalasem.models.BaseResponse;
import com.aait.aalasem.models.CheckCodeResponse;
import com.aait.aalasem.models.ConditionsResponse;
import com.aait.aalasem.models.DeadResponse;
import com.aait.aalasem.models.DictionaryResponse;
import com.aait.aalasem.models.ForgetResponse;
import com.aait.aalasem.models.GetJobRequestResponse;
import com.aait.aalasem.models.GetJobsResponse;
import com.aait.aalasem.models.GetOrdersResponse;
import com.aait.aalasem.models.GetPacientsResponse;
import com.aait.aalasem.models.GetProductionResponse;
import com.aait.aalasem.models.JobDetailsResponse;
import com.aait.aalasem.models.LatestJobResponse;
import com.aait.aalasem.models.Login;
import com.aait.aalasem.models.LoginResponse;
import com.aait.aalasem.models.MonasbaResponse;
import com.aait.aalasem.models.MonasbatResponse;
import com.aait.aalasem.models.MyOrdersResponse;
import com.aait.aalasem.models.NewDetailsResponse;
import com.aait.aalasem.models.NewsResponse;
import com.aait.aalasem.models.NotificationCount;
import com.aait.aalasem.models.NotificationResponse;
import com.aait.aalasem.models.OrderDetailsResponse;
import com.aait.aalasem.models.PatientDatailsResponse;
import com.aait.aalasem.models.ProductionDetailsResponse;
import com.aait.aalasem.models.ProfileResponse;
import com.aait.aalasem.models.SocialResponse;
import com.aait.aalasem.models.StaticticsResponse;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface ServiceApi {

    @POST(Urls.Register)
    Call<LoginResponse> register(
            @Query("first_name") String first_name,
            @Query("last_name") String last_name,
            @Query("phone") String phone,
            @Query("password") String password,
            @Query("in_dictionary") String in_dictionary,
            @Query("location") String location,
            @Query("jop") String jop,
            @Query("device_type") String device_type,
            @Query("device_id") String device_id,
            @Query("social_status") String social_status,
            @Query("sons_count") String sons_count,
            @Query("educational_qualification") String educational_qualification,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("middle_name") String middle_name
            );
    @POST(Urls.Login)
    Call<Login> login(
            @Query("phone") String phone,
            @Query("password") String password,
            @Query("device_type") String device_type,
            @Query("device_id") String device_id
    );
    @POST(Urls.Logout)
    Call<BaseResponse> logout(
            @Header("Authorization") String Authorization
    );
    @GET(Urls.Conditions)
    Call<ConditionsResponse> getConditons();
    @GET(Urls.About)
    Call<ConditionsResponse> getAbout();
    @POST(Urls.ForGetPass)
    Call<ForgetResponse> forgotPass(
            @Query("phone") String phone
    );
    @POST(Urls.ResendCode)
    Call<ForgetResponse> resend(
            @Query("id") int id
    );
    @POST(Urls.check_code)
    Call<CheckCodeResponse> checkCode(
            @Query("id") int id,
            @Query("code") String code
    );
    @POST(Urls.ResetPass)
    Call<BaseResponse> resetPass(
            @Query("id") int id,
            @Query("password") String password
    );
    @GET(Urls.Statistics)
    Call<StaticticsResponse> getStatistic();
    @GET(Urls.Deades)
    Call<DeadResponse> getDeads();
    @GET(Urls.Dictionary)
    Call<DictionaryResponse> getDictionary();
    @GET(Urls.Search)
    Call<DictionaryResponse> search(
            @Query("keyword") String keyword
    );
    @GET(Urls.ContactUsWord)
    Call<ConditionsResponse> getContact();
    @GET(Urls.News)
    Call<NewsResponse> getNews();
    @GET(Urls.Patients)
    Call<GetPacientsResponse> getPacients();
    @GET(Urls.Jobs)
    Call<GetJobsResponse> getJobs();
    @GET(Urls.JobDetails)
    Call<JobDetailsResponse> getJob(@Query("id") int id);
    @GET(Urls.Occasions)
    Call<MonasbatResponse> getMonasbat();
    @GET(Urls.Monasba)
    Call<MonasbaResponse> getMonasba(@Query("id") int id);
    @GET(Urls.Orders)
    Call<GetOrdersResponse> getOrders();
    @GET(Urls.Order)
    Call<OrderDetailsResponse> getOrder(@Query("id") int id);
    @GET(Urls.Profile)
    Call<ProfileResponse> getProfile(@Header("Authorization") String Authorization);
    @POST(Urls.UpdateProfile)
    Call<ProfileResponse> updateProfile(
            @Header("Authorization") String Authorization,
            @Query("first_name") String first_name,
            @Query("last_name") String last_name,
            @Query("phone") String phone,
            @Query("password") String password,
            @Query("in_dictionary") String in_dictionary,
            @Query("location") String location,
            @Query("jop") String jop,
            @Query("social_status") String social_status,
            @Query("sons_count") String sons_count,
            @Query("educational_qualification") String educational_qualification,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("middle_name") String middle_name
    );
    @Multipart
    @POST(Urls.UpdateProfile)
    Call<ProfileResponse> updateProfilewithImage(
            @Header("Authorization") String Authorization,
            @Query("first_name") String first_name,
            @Query("last_name") String last_name,
            @Query("phone") String phone,
            @Query("password") String password,
            @Query("in_dictionary") String in_dictionary,
            @Query("location") String location,
            @Query("jop") String jop,
            @Query("social_status") String social_status,
            @Query("sons_count") String sons_count,
            @Query("educational_qualification") String educational_qualification,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Query("middle_name") String middle_name,
            @Part MultipartBody.Part image
    );
    @GET(Urls.Production)
    Call<GetProductionResponse> getProduction(@Query("type") String type);
    @GET(Urls.Prod)
    Call<ProductionDetailsResponse> getProd(@Query("id") int id);
    @GET(Urls.LatestJob)
    Call<LatestJobResponse> getLatest();
    @GET(Urls.JobRegests)
    Call<GetJobRequestResponse> getJobRequest();
    @POST(Urls.AddOrder)
    Call<BaseResponse> addOrder(
            @Header("Authorization") String Authorization,
            @Query("name") String name,
            @Query("phone") String phone,
            @Query("details") String details
    );
    @GET(Urls.MyOrders)
    Call<MyOrdersResponse> getMyOrders(
            @Header("Authorization") String Authorization
    );
    @POST(Urls.Contact)
    Call<BaseResponse> contact(
            @Header("Authorization") String Authorization,
            @Query("name") String name,
            @Query("key") String key,
            @Query("title") String title,
            @Query("content") String content
    );
    @Multipart
    @POST(Urls.AddPatient)
    Call<BaseResponse> addPatient(
            @Header("Authorization") String Authorization,
            @Query("name") String name,
            @Query("body") String body,
            @Query("extra") String extra,
            @Part MultipartBody.Part image
    );
    @POST(Urls.AddPatient)
    Call<BaseResponse> AddPatient(
            @Header("Authorization") String Authorization,
            @Query("name") String name,
            @Query("body") String body,
            @Query("extra") String extra
    );
    @GET(Urls.NewDetails)
    Call<NewDetailsResponse> getNew(@Query("id") int id);
    @Multipart
    @POST(Urls.AddNew)
    Call<BaseResponse> addNew(
            @Header("Authorization") String Authorization,
            @Query("title") String name,
            @Query("body") String body,
            @Part List<MultipartBody.Part> images
    );
    @POST(Urls.AddNew)
    Call<BaseResponse> AddNew(
            @Header("Authorization") String Authorization,
            @Query("title") String name,
            @Query("body") String body
    );
    @Multipart
    @POST(Urls.AddProduction)
    Call<BaseResponse> addProduction(
            @Header("Authorization") String Authorization,
            @Query("title") String name,
            @Query("body") String body,
            @Part MultipartBody.Part image
    );
    @POST(Urls.AddProduction)
    Call<BaseResponse> AddProduction(
            @Header("Authorization") String Authorization,
            @Query("title") String name,
            @Query("body") String body
    );
    @Multipart
    @POST(Urls.AddMonasba)
    Call<BaseResponse> addMonasba(
            @Header("Authorization") String Authorization,
            @Query("title") String title,
            @Query("body") String body,
            @Query("lat") String lat,
            @Query("lng") String lng,
            @Part MultipartBody.Part image
    );
    @POST(Urls.AddMonasba)
    Call<BaseResponse> AddMonasba(
            @Header("Authorization") String Authorization,
            @Query("title") String title,
            @Query("body") String body,
            @Query("lat") String lat,
            @Query("lng") String lng
    );
    @GET(Urls.Notification)
    Call<NotificationResponse> getNotification(
            @Header("Authorization") String Authorization
    );
    @POST(Urls.DeleteNotification)
    Call<BaseResponse> delete(
            @Header("Authorization") String Authorization,
            @Query("_method") String _method,
            @Query("id") int id
    );
    @GET(Urls.Count)
    Call<NotificationCount> getCount(@Header("Authorization") String Authorization);
    @GET(Urls.Bank)
    Call<BanckResponse> getBank(@Header("Authorization") String Authorization);
    @Multipart
    @POST(Urls.Pay)
    Call<BaseResponse> pay(
            @Header("Authorization") String Authorization,
            @Query("name") String name,
            @Query("persons") int  persons,
            @Query("number") String number,
            @Query("cost") String cost,
            @Part MultipartBody.Part image
    );
    @Multipart
    @POST(Urls.AddJobRequest)
    Call<BaseResponse> addJobRequest(
            @Header("Authorization") String Authorization,
            @Query("title") String title,
            @Query("body") String body,
            @Part MultipartBody.Part file
    );
    @GET(Urls.Social)
    Call<SocialResponse> getSocial();
    @GET(Urls.PatientDetails)
    Call<PatientDatailsResponse> getPatient(@Query("id") int id);

    @POST(Urls.Verify)
    Call<BaseResponse> verifying(@Query("id") int id,
                                 @Query("code") String code);


}
